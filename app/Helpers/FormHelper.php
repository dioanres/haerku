<?php
namespace App\Helpers;
use Form;

class FormHelper
{
    public static function form($type, $label, $id, $attr = [], $class = [])
    {
    	if($type == 'datepicker'){
    		echo '
    			<div class="form-group">
                    <label class="control-label" for="$id">'.$label.'</label>
                    <div class="input-group">
                        <input type="text" name="'.$id.'" id="'.$id.'" class="form-control datetimepicker '.implode(" ",$class).'" '.implode(" ",$attr).' />
                    </div>
                </div>
    		';
    	}else if($type == 'number'){
            echo '
                <div class="form-group">
                    <label class="control-labels" for="'.$id.'">'.$label.'</label>
                    '. Form::number($id, null, ['id' => $id, 'class' => 'form-control '.implode(" ",$class), implode(" ",$attr)]) .'
                </div>
            ';
    		/*echo '
    			<div class="form-group">
                    <label class="control-label" for="'.$id.'">'.$label.'</label>
                    <div class="">
                        <input type="number" name="'.$id.'" value="0" id="'.$id.'" class="form-control '.implode(" ",$class).'" '.implode(" ",$attr).' />
                    </div>
                </div>
    		';*/
    	} else if($type == 'textarea'){
            echo '
                <div class="form-group">
                    <label class="control-label" for="'.$id.'">'.$label.'</label>
                    <div class="">
                        <textarea class="form-control resize_vertical '.implode(" ",$class).'" id="'.$id.'" name="'.$id.'" rows="7" '.implode(" ",$attr).'></textarea>
                    </div>
                </div>
            ';
        } else if($type == 'text'){
            echo '
                <div class="form-group">
                    <label class="control-labels" for="'.$id.'">'.$label.'</label>
                    '. Form::text($id, null, ['id' => $id, 'class' => 'form-control '.implode(" ",$class), implode(" ",$attr)]) .'
                </div>
            ';
            /*echo '
                <div class="form-group">
                    <label class="control-label" for="$id">'.$label.'</label>
                    <div class="input-group">
                        <input type="text" name="'.$id.'" id="'.$id.'" class="form-control" '.implode(" ",$attr).'/>
                    </div>
                </div>
            ';*/
        } else if($type == 'file'){
            echo '
                <div class="form-group">
                    <label class="control-label" for="$id">'.$label.'</label>
                    <div class="input-group">
                        <input type="file" name="'.$id.'" id="'.$id.'" class="form-control" '.implode(" ",$attr).' />
                    </div>
                </div>
            ';
        }
    }
}
