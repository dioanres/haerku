<?php

namespace Modules\Master\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Master\Entities\{JobModel,JobLevelsModel};
use DB;
use Flash;

class JobLevelController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
      protected $joblevel;
    public function __construct()
    {
        $this->joblevel = new JobLevelsModel();
    } 

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $data['job'] = JobModel::all();
        $data['job_levels'] = JobLevelsModel::all();
        return view('master::job.index-job', $data);
    }

    public function create()
    {
        return view('master::job.create-job-level');
    }

    public function store(Request $request)
    {
       $input = $request->all();
        if($this->joblevel->create($input)) {
            return redirect(route('master.job.level'));
        } else {
            return redirect(route('master.job.level.create'));
        }
    }

    public function edit($id)
    {
        $data['job_levels'] = $this->joblevel->where('job_level_id',$id)->first();
        $data['status'] = $data['job_levels']->status;
        return view('master::job.edit-job-level',$data);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $update = $this->joblevel->where('job_level_id',$id)->update([
            'job_level_desc' => $data['job_level_desc'],
            'status' => $data['status']
        ]);
        
        if($update) {
            return redirect(route('master.job.level'));
        } else {
            return redirect()->back();
        }

    }

    public function destroy($id)
    {
        JobLevelsModel::destroy($id);
        return redirect(route('master.job.level'));
    }
}
