<?php

namespace Modules\Master\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Master\Entities\{JobModel, JobLevelsModel, Mst_Department};
use DB;
use Flash;

class JobController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $job;
    public function __construct()
    {
        $this->job = new JobModel();
    } 

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */

    public function index()
    {
        $data['job'] = JobModel::all();
        $data['job_levels'] = JobLevelsModel::all();
        return view('master::job.index-job',$data);
    }

    public function create()
    {
        $data['curr_job_level_id'] = null;
        $data['curr_dept_id'] = null;
        $data['status'] = true;
        $data['job_levels'] = JobLevelsModel::pluck('job_level_desc', 'job_level_id');
        $data['dept'] = Mst_Department::pluck('dept_desc', 'dept_id');
        return view('master::job.create-job', $data);
    }

    public function store(Request $request)
    {
       $input = $request->all();
       $input['job_level_id'] = $input['job_level_id'][0];
       $input['dept_id'] = $input['dept_id'][0];
        if($this->job->create($input)) {
            return redirect(route('master.job'));
        } else {
            return redirect(route('master.job.create'));
        }
    }

    public function edit($id)
    {
        $data['job'] = $this->job->where('id_job',$id)->first();
        $data['curr_job_level_id'] = $data['job']->job_level_id;
        $data['curr_dept_id'] = $data['job']->dept_id;
        $data['status'] = $data['job']->status;
        $data['job_levels'] = JobLevelsModel::pluck('job_level_desc', 'job_level_id');
        $data['dept'] = Mst_Department::pluck('dept_desc', 'dept_id');
        return view('master::job.edit-job',$data);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $update = $this->job->where('id_job',$id)->update([
            'job_desc' => $data['job_desc'],
            'job_level_id' => $data['job_level_id'][0],
            'dept_id' => $data['dept_id'][0],
            'status' =>  $data['status'] 
        ]);
        
        if($update) {
            return redirect(route('master.job'));
        } else {
            return redirect()->back();
        }

    }

    public function destroy($id)
    {
        JobModel::destroy($id);
        return redirect(route('master.job'));
    }
}
