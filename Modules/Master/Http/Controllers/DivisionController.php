<?php

namespace Modules\Master\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Master\Entities\{Mst_Division,CompaniesModel};
use DB;
use Flash;

class DivisionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('auth');
    } 

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $data['division']=[];
        //$data['department'] = Mst_Department::all();
        $data['company'] = CompaniesModel::all();
        return view('master::division.index-division', $data);
    }

    public function create()
    {   
        $data['company'] = CompaniesModel::all();
        return view('master::division.create-division',$data);
    }

    public function store(Request $request){

        $result = Mst_Division::create([
            'division_desc' => $request->division_desc,
            'id_company' => $request->get('company_id')
        ]);

        if($result) {
            return redirect(route('division'));
        } else {
            return redirect(route('master.division.create'));
        }
    }

    public function search(Request $request)
    {
        $data['company'] = CompaniesModel::all();
        $company_id =  $request->get('companyId');
        $data['division'] = Mst_Division::where('id_company',$company_id)->get();

        return view('master::division.index-division', $data); 
            
    }

    public function edit($id)

    {
        $data = Mst_Division::join('mst_company','mst_division.id_company','=','mst_company.id_company')
                    ->select('mst_division.division_id','mst_company.company_name','mst_division.division_desc')
                    ->where('mst_division.division_id',$id)->first();

        //echo $data;

        return view('master::division.edit-division',compact('data'));
    }

    public function update(Request $request, $id)
    {
        //$affectedRows = User::where('votes', '>', 100)->update(['status' => 2]);
        $data = $request->all();
        //echo $data['division_desc'];
        
        $update = Mst_Division::where('division_id',$id)->update([
            'division_desc' => $data['division_desc']
        ]);
        
        if($update) {
            return redirect(route('division'));
        } else {
            return redirect()->back();
        }
        

    }

    public function delete($id)
    {
        
        $update = Mst_Division::where('division_id',$id)->update([
            'deleted_at' => now()
        ]);
        
        if($update) {
            return redirect(route('division'));
        } else {
            return redirect()->back();
        }
        

    }

}
