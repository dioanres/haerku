<?php

namespace Modules\Master\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Master\Entities\{Mst_Religion,Mst_Identity_Type,Mst_Marital_Status,Mst_Division,Mst_Department,JobLevelsModel,Mst_Employee_Status,JobModel,CompaniesModel};
use Illuminate\Routing\Controller;
use Modules\Master\Entities\EmployeesModel;
use Modules\Master\Repositories\{EmployeesRepository, EmployeeJobRepository};
use DateTime;

class EmployeesController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function __construct(EmployeesRepository $employeesRepo, EmployeeJobRepository $employeeJobRepo)
    {
        $this->employeesRepository = $employeesRepo;
        $this->employeeJobRepository = $employeeJobRepo;
    }

    public function index()
    {
        $data['list'] =  $this->employeesRepository->list();
        return view('master::employee.index_employees_info', $data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $data['employees'] = null;
        $data['comp'] = CompaniesModel::pluck('company_name', 'id_company');
        $data['religion'] = Mst_Religion::pluck('religion_desc','religion_id');
        $data['identity_type'] = Mst_Identity_Type::pluck('identity_type', 'identity_type_id');
        $data['marital_status'] = Mst_Marital_Status::pluck('marital_status_desc', 'marital_status_id');
        $data['division'] = Mst_Division::pluck('division_desc', 'division_id');
        $data['departement'] = Mst_Department::pluck('dept_desc', 'dept_id');
        $data['job_levels'] = JobLevelsModel::pluck('job_level_desc', 'job_level_id');
        $data['employee_status'] = Mst_Employee_Status::pluck('empl_status_desc', 'empl_status_id');
        $data['job'] = JobModel::pluck('job_desc', 'id_job');
        return view('master::employee.create_employees_info',$data);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
       $input = $request->all();
       $input['date_of_birth'] = $this->formatDate($input['date_of_birth']);
       $input['join_date'] = $this->formatDate($input['join_date']);
       $input['end_date'] = $this->formatDate($input['end_date']);
         // dd($input);
       $this->employeesRepository->create($input);
       return redirect(route('master.employees'));
   }

   public function formatDate($value)
   {
       $date = new DateTime($value);
       return $date->format('Y-m-d');
   }
    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
       $data['employees'] =  $this->employeesRepository->getEmplJoinEmplJob($id);
       $data['religion'] = Mst_Religion::all();
       $data['identity_type'] = Mst_Identity_Type::all();
       $data['marital_status'] = Mst_Marital_Status::all();
       $data['division'] = Mst_Division::all();
       $data['departement'] = Mst_Department::all();
       $data['job_levels'] = JobLevelsModel::all();
       $data['employee_status'] = Mst_Employee_Status::all(); 
       $data['job'] = JobModel::all();
       $data['company'] = CompaniesModel::all();
         // dd($data);
       return view('master::employee.edit_employees_info',$data);
   }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
      $data = $request->all();
      $data['date_of_birth'] = $this->formatDate($data['date_of_birth']);
      $data['joint_date'] = $this->formatDate($data['joint_date']);
      $data['end_date'] = $this->formatDate($data['end_date']);
      $this->employeesRepository->update($data, $id);
      return redirect(route('master.employees'));
  }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
      $this->employeesRepository->deleteEmployee($id);
      return redirect(route('master.employees'));
    }
}
