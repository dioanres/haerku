<?php

namespace Modules\Master\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Master\Entities\CompaniesModel;
use Modules\Master\Repositories\CompanyRepository;

class CompaniesController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    protected $company;
    private $companyRepository; 

    public function __construct(CompanyRepository $companyRepo)
    {
        $this->company = new CompaniesModel();
        $this->companyRepository = $companyRepo;
    }

    public function index()
    {
        $list = $this->companyRepository->all();

        return view('master::companies.index',compact('list'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('master::companies.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        
        //
        $input = $request->all();

        if($this->company->create($input)) {
            return redirect(route('master.companies'));
        } else {
            return redirect(route('master.companies.create'));
        }

    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('master::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $data = $this->company->where('id_company',$id)->first();

        return view('master::companies.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //$affectedRows = User::where('votes', '>', 100)->update(['status' => 2]);
        $data = $request->all();
        
        $update = $this->company->where('id_company',$id)->update([
            'company_name' => $data['company_name'],
            'address' => $data['address'],
            'total_employee' => $data['total_employee'],
            'no_telp' => $data['no_telp']
        ]);
        
        if($update) {
            return redirect(route('master.companies'));
        } else {
            return redirect()->back();
        }
        

    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
