<?php

namespace Modules\Master\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Master\Entities\{MstGlobalModel, CompaniesModel, MstGlobalDtlModel};
use DB;
use Flash;

class GlobalController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $global;

    public function __construct()
    {
        $this->global = new MstGlobalModel();
    } 

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $data['list'] = MstGlobalModel::all();
        return view('master::global.index', $data);
    }

    public function create()
    {
    	$data['company'] = CompaniesModel::all();
        // $data['param_type'] = MstGlobalModel::all();
        return view('master::global.create', $data);
    }

    public function store(Request $request)
    {
        $input = $request->all();
        // var_dump($this->global->create($input)->global_id);
        // exit();
        $id = $this->global->create($input)->global_id;
        if($id) {
            return redirect(route('global.edit' ,['id' => $id]));
        } else {
            return redirect()->back();
        }

    }

    public function edit($id)
    {   
        $data['param'] = MstGlobalModel::where('global_id',$id)->first();
        $data['list'] = MstGlobalDtlModel::where('global_id', $id)->get();
        $data['company'] = CompaniesModel::all();
        // dd($data);
        return view('master::global.edit', $data);
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        
        $update = MstGlobalModel::where('global_id',$id)->update([
            'param_type' => $data['param_type'],
            'status' =>  $data['status'] 
        ]);
        
        if($update) {
            return redirect(route('global'));
        } else {
            return redirect()->back();
        }
    }

    public function storeDtl(Request $request)
    {
        $input = $request->all();

        if($input['global_dtl_id']){
            $create = MstGlobalDtlModel::where('global_dtl_id',$input['global_dtl_id'])->update([
                'param_id' => $input['param_id'],
                'param_desc' =>  $input['param_desc'],
                'status' =>  $input['status']
            ]);
        } else {
           $create = MstGlobalDtlModel::create($input);
        }
        

        if($create) {
            return redirect(route('global.edit' ,['id' => $input['global_id']]));
        } else {
            return redirect()->back();
        }

    }

    public function destroy($id)
    {
        MstGlobalModel::destroy($id);
        return redirect(route('global'));
    }

    public function destroyDtl($id)
    {
        MstGlobalDtlModel::destroy($id);
        return redirect()->back();
        //return redirect(route('global.edit' ,['id' => $id]));
    }

}
