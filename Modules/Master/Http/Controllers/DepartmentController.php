<?php

namespace Modules\Master\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Master\Entities\{Mst_Department,Mst_Division};
use DB;
use Flash;

class DepartmentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    } 

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $data['department'] = Mst_Department::all();
        return view('master::department.index-department', $data);
    }

    public function create($id)
    {   
        $data = Mst_Division::select('division_id','division_desc')->where('division_id',$id)->first();
    	// $data['mstPermitType'] = Mst_Permit_Type::list();
        return view('master::department.create-department',compact('data'));
    }

    public function search($id)
    {
        $data['department'] = Mst_Department::where('division_id',$id)->get();
        return view('master::department.index-department', $data); 
    }

    public function store(Request $request,$id){

        $result = Mst_Department::create([
            'dept_desc' => $request->dept_desc,
            'division_id' => $id
        ]);


        if($result) {
            $data['department'] = Mst_Department::where('division_id',$id)->get();
            return view('master::department.index-department', $data);
        } else {
            $data = Mst_Division::where('division_id',$id)->first();
            return view('master::department.create-department',compact('data'));
        }
    }

    public function edit($id)
    {
        $data = Mst_Department::join('mst_division','mst_departement.division_id','=','mst_division.division_id')
                    ->select('mst_departement.dept_id','mst_division.division_desc','mst_departement.dept_desc')
                    ->where('mst_departement.dept_id',$id)->first();

        //echo $data;

        return view('master::department.edit-department',compact('data'));
    }
    public function update(Request $request, $id)
    {
        //$affectedRows = User::where('votes', '>', 100)->update(['status' => 2]);
        $data = $request->all();
        //echo $data['division_desc'];
        
        $update = Mst_Department::where('dept_id',$id)->update([
            'dept_desc' => $data['dept_desc']
        ]);
        
        if($update) {
            return redirect(route('department'));
        } else {
            return redirect()->back();
        }
    }

    public function delete($id)
    {
        
        $update = Mst_Department::where('dept_id',$id)->update([
            'deleted_at' => now()
        ]);
        
        if($update) {
            return redirect(route('department'));
        } else {
            return redirect()->back();
        }
        

    }
}
