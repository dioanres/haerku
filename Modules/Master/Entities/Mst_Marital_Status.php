<?php

namespace Modules\Master\Entities;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use Session;
use DB;


class Mst_Marital_Status extends Model
{
    // use SoftDeletes;
    public $table = 'mst_marital_status';

    public $fillable = [
        'marital_status_id',
        'marital_status_desc',
        'deleted_at',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by'
    ];
    protected $primaryKey = 'marital_status_id';
}
