<?php

namespace Modules\Master\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Session;
use DB;

class MstGlobalDtlModel extends Model
{
    use SoftDeletes;
    protected $table = 'mst_global_dtl';

    protected $fillable = [
        'global_dtl_id',
        'global_id',
        'param_id',
        'param_desc',
        'deleted_at',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
        'status'
    ];

    protected $primaryKey = 'global_dtl_id';

}
