<?php

namespace Modules\Master\Entities;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use Session;
use DB;


class Mst_Identity_Type extends Model
{
    // use SoftDeletes;
    public $table = 'mst_identity_type';

    public $fillable = [
        'identity_type_id',
        'identity_type',
        'deleted_at',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by'
    ];
    protected $primaryKey = 'identity_type_id';
}
