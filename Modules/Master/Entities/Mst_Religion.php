<?php

namespace Modules\Master\Entities;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use Session;
use DB;


class Mst_Religion extends Model
{
    // use SoftDeletes;
    public $table = 'mst_religion';

    public $fillable = [
        'religion_id',
        'religion_desc',
        'deleted_at',
        'created_by',
        'updated_by'
    ];

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'updated_date';
    protected $primaryKey = 'religion_id';
}
