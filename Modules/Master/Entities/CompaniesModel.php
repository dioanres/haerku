<?php

namespace Modules\Master\Entities;

use Illuminate\Database\Eloquent\Model;

class CompaniesModel extends Model
{
    protected $table = 'mst_company';

    protected $fillable = [
        'company_name',
        'address',
        'total_employee',
        'no_telp'
    ];

    
}
