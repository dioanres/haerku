<?php

namespace Modules\Master\Entities;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use Session;
use DB;


class EmployeeJobModel extends Model
{
    use SoftDeletes;
    public $table = 'employee_job';

    public $fillable = [
        'id_empl_job',
        'id_employee',
        'id_job',
        'start_date',
        'end_date',
        'created_date',
        'created_by',
        'updated_date',
        'updated_by',
        'deleted_at'
    ];

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'updated_date';
    protected $primaryKey = 'id_empl_job';

    public function employee()
    {
        return $this->belongsTo('Modules\Master\Entities\EmployeesModel', 'id_employee', 'id_employee');
    }

    public function job()
    {
        return $this->belongsTo('Modules\Master\Entities\JobModel', 'id_job');
    }

    public static function create($input, $id_employee){
        return DB::table('employee_job')->insert([
            'id_employee' =>  $id_employee,
            'id_job'  => $input['id_job'][0],        
            'start_date' => $input['join_date'],           
            'end_date' => $input['end_date'],
            'created_date' => now(),
            'updated_date' => now()
        ]);
    }

    public static function updateEmployeeJob($input, $id){
        return DB::table('employee_job')->where('id_employee', $id)->update([
            // 'id_employee' => $input['id_employee'],
            'id_job'  => $input['id_job'],        
            'start_date' => $input['joint_date'],           
            'end_date' => $input['end_date'],
            'updated_date' => now()
        ]);
    }

    public static function deleteEmployeeJob($id){
        return DB::table('employee_job')->where('id_employee', $id)->delete();
    }

}
