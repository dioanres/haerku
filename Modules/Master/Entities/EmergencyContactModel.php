<?php

namespace Modules\Master\Entities;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use Session;
use DB;


class EmergencyContactModel extends Model
{
    use SoftDeletes;
    public $table = 'emergency_contact';

    public $fillable = [
        'ec_id',
        'id_employee',
        'relation_type_code',
        'ec_name',
        'hp_no',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
        'ec_address'
    ];
    protected $primaryKey = 'ec_id';

    public static function create($input, $id_employee){
        return DB::table('emergency_contact')->insert([
            'id_employee' =>  $id_employee,
            'relation_type_code'  => $input['relation_type_code'][0],        
            'ec_name' => $input['empl_ec_name'],           
            'hp_no' => $input['empl_ec_phone'],
            'created_at' => now(),
            'updated_at' => now(),
            'ec_address' => $input['ec_address']
        ]);
    }
}
