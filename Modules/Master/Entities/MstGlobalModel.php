<?php

namespace Modules\Master\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Session;
use DB;


class MstGlobalModel extends Model
{
    use SoftDeletes;
    protected $table = 'mst_global';

    protected $fillable = [
        'global_id',
        'param_type',
        'id_company',
        'deleted_at',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
        'status'
    ];

    protected $primaryKey = 'global_id';

}
