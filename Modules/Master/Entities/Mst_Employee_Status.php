<?php

namespace Modules\Master\Entities;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use Session;
use DB;


class Mst_Employee_Status extends Model
{
    // use SoftDeletes;
    public $table = 'mst_employee_status';

    public $fillable = [
        'empl_status_id',
        'empl_status_desc',
        'deleted_at',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by'
    ];
    protected $primaryKey = 'empl_status_id';
}
