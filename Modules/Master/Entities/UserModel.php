<?php

namespace Modules\Master\Entities;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Hash;
use Session;
use DB;


class UserModel extends Model
{
    use SoftDeletes;
    public $table = 'users';

    public $fillable = [
        'id',
        'name',
        'email',
        'password',
        'remember_token',
        'created_at',
        'updated_at',
        'email_verified_at',
        'id_company',
        'update_password',
        'deleted_at'
    ];
    protected $primaryKey = 'id';

    public static function create($input){
        return DB::table('users')->insertGetId([
            'name'  => $input['email'],        
            'password' => Hash::make($input['pass']),
            'email'  => $input['email'],  
            'id_company' => $input['id_company'][0],
            'created_at' => now(),
            'updated_at' => now()
        ]);
    }

     public static function deleteUser($id){
        return DB::table('users')->where('email', $id)->delete();
    }

}
