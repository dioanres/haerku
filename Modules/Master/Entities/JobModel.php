<?php

namespace Modules\Master\Entities;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use Session;
use DB;


class JobModel extends Model
{
    use SoftDeletes;
    public $table = 'mst_jobs';

    public $fillable = [
        'id_job',
        'job_desc',
        'job_level_id',
        'dept_id',
        'deleted_at',
        'created_date',
        'created_by',
        'updated_date',
        'updated_by',
        'id_company',
        'status'
    ];
    protected $primaryKey = 'id_job';
    
    public function getJobById()
    {
        return $this->hasOne('Modules\Master\Entities\EmployeeJobModel', 'id_job');
    }
}
