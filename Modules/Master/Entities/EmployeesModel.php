<?php

namespace Modules\Master\Entities;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use Session;
use DB;

class EmployeesModel extends Model
{
	use SoftDeletes;
	public $table = 'employee';

	protected $fillable = [
		'id_employee',
		'fullname',
		'place_of_birth',
		'date_of_birth',
		'religion_id',
		'marital_status_id',
		'gender',
		'mother_maiden_name',
		'email',
		'hp_no',
		'telp_no',
		'empl_status_id',
		'empl_type_id',
		'join_date',
		'resign_date',
		'created_date',
		'created_by',
		'updated_date',
		'updated_by',
		'comp_id',
		'id_user',
		'nik',
		'id_role',
		'deleted_at',
		'blood_type',
		'current_address'];

		const CREATED_AT = 'created_date';
		const UPDATED_AT = 'updated_date';
		protected $primaryKey = 'id_employee';
		
		public function employeeJob()
		{
			return $this->hasOne('Modules\Master\Entities\EmployeeJobModel', 'id_employee');
		}

		public static function list()
		{
			return DB::select("SELECT a.id_employee, a.nik, a.fullname, e.dept_desc, d.job_desc, f.company_name from employee a LEFT JOIN employee_job b on a.id_employee = b.id_employee LEFT JOIN employee_job c on a.id_employee = c.id_employee LEFT JOIN mst_jobs d on c.id_job = d.id_job LEFT JOIN mst_departement e on d.dept_id = e.dept_id LEFT JOIN mst_company f on a.comp_id = f.id_company
				where a.deleted_at is null;");
		}
		
		public static function create($input, $id_user){
			return DB::table('employee')->insertGetId([
				'fullname'  => $input['fullname'],
				'place_of_birth' => $input['place_of_birth'],
				'date_of_birth' => $input['date_of_birth'],					
				'religion_id' => $input['religion_id'][0],
				'marital_status_id' => $input['marital_status_id'][0],
				'gender' => $input['gender'][0],
				// 'mother_maiden_name' => $input['mother_maiden_name'],
				'email' => $input['email'],
				'hp_no' => $input['hp_no'],
				'empl_status_id' => $input['empl_status_id'][0],
				'empl_type_id' => $input['identity_type_id'][0],	
				'join_date' => $input['join_date'],
				'comp_id' => $input['id_company'][0],
				'id_user' => $id_user,
				'nik' => $input['nik'],
				// 'id_role' => $input['id_role'],
				'blood_type' => $input['blood_type'],
				'current_address' => $input['current_address'],
				'id_address' => $input['id_address']
			]);
		}
		public static function updateEmployee($input, $id){
			return DB::table('employee')->where('id_employee', $id)->update([
				'fullname'  => $input['fullname'],
				'place_of_birth' => $input['place_of_birth'],
				'date_of_birth' => $input['date_of_birth'],					
				'religion_id' => $input['religion_id'],
				'marital_status_id' => $input['marital_status_id'],
				'gender' => $input['gender'],
				// 'mother_maiden_name' => $input['mother_maiden_name'],
				// 'email' => $input['email'],
				'hp_no' => $input['hp_no'],
				'empl_status_id' => $input['empl_status_id'],
				'empl_type_id' => $input['empl_type_id'],	
				'join_date' => $input['join_date'],
				'comp_id' => $input['comp_id'],
				// 'id_user' => $input['id_user'],
				'nik' => $input['nik'],
				// 'id_role' => $input['id_role'],
				'blood_type' => $input['blood_type'],
				'current_address' => $input['current_address'],
				'id_address' => $input['id_address']
			]);
		}
	}