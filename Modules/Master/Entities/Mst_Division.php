<?php

namespace Modules\Master\Entities;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use Session;
use DB;


class Mst_Division extends Model
{
    // use SoftDeletes;
    public $table = 'mst_division';

    public $fillable = [
        'division_id',
        'division_desc',
        'deleted_at',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
        'id_company'
    ];
    protected $primaryKey = 'devision_id';
}
