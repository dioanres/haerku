<?php

namespace Modules\Master\Entities;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use Session;
use DB;


class JobLevelsModel extends Model
{
    use SoftDeletes;
    public $table = 'mst_job_levels';

    public $fillable = [
        'job_level_id',
        'job_level_desc',
        'deleted_at',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
        'status'
    ];
    protected $primaryKey = 'job_level_id';
}
