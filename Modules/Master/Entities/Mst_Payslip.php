<?php

namespace Modules\Master\Entities;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use Session;
use DB;


class Mst_Payslip extends Model
{
    // use SoftDeletes;
    public $table = 'mst_payslip';

    public $fillable = [
        'payslip_code',
        'payslip_desc',
        'type',
        'percent_fee',
        'amount_fee',
        'deleted',
        'created_by',
        'updated_by',
    ];

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'updated_date';
    protected $primaryKey = 'payslip_code';
    protected $keyType = 'string';
    public $incrementing = false;
}
