<?php

namespace Modules\Master\Entities;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use Session;
use DB;


class Mst_Department extends Model
{
    // use SoftDeletes;
    public $table = 'mst_departement';

    public $fillable = [
        'dept_id',
        'dept_desc',
        'division_id',
        'deleted_at',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by'
    ];
    protected $primaryKey = 'dept_id';
}
