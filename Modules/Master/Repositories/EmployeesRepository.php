<?php

namespace Modules\Master\Repositories;

use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;
use Modules\Master\Entities\{EmployeesModel, EmployeeJobModel, UserModel, EmergencyContactModel};
use Modules\Master\Repositories\EmployeeJobRepository;
use DateTime;
use DB;
/**
 * Class CompanyRepository.
 */
class EmployeesRepository extends BaseRepository
{
    /**
     * @return string
     *  Return the model
     */
     public function __construct(EmployeeJobRepository $emplJobRepo)
    {
        $this->emplJobRepo = $emplJobRepo;
    }
    public function model()
    {
    	return EmployeesModel::class;
    }

    public function list()
    {
    	return EmployeesModel::list();
    }

    public function getEmployeeByid($id)
    {
        return $this->getById($id);
    }

     public function getEmplJoinEmplJob($id)
    {
        return EmployeesModel::with('employeeJob', 'employeeJob.job')->find($id);
    }

    public function create(array $input)
    {
    	return DB::transaction(function() use ($input)
    	{            
            $input['currentDate'] = new DateTime();
            $input['pass']= $input['currentDate']->format('YmHi');
            // dd($input);
            $id_user = UserModel::create($input);
    		$id_employee = EmployeesModel::create($input, $id_user);
    		EmployeeJobModel::create($input, $id_employee);    	
            EmergencyContactModel::create($input, $id_employee);	
    	});
    }

    public function update(array $input, $id)
    {
        return DB::transaction(function() use ($input, $id)
        {
            EmployeesModel::updateEmployee($input, $id);
            EmployeeJobModel::updateEmployeeJob($input, $id);
        });
    }

    public function deleteEmployee($id)
    {
        return DB::transaction(function() use ($id)
        {
            $email = EmployeesModel::select('email')->where('id_employee', $id)->get();
            $this->deleteById($id);
            $this->emplJobRepo->deleteEmployeeJob($id);
            // UserModel::deleteUser($email);
        });
    }
}
