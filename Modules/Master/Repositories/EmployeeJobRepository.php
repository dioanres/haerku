<?php

namespace Modules\Master\Repositories;

use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;
use Modules\Master\Entities\{EmployeesModel, EmployeeJobModel, UserModel};
use DB;
/**
 * Class CompanyRepository.
 */
class EmployeeJobRepository extends BaseRepository
{
    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
    	return EmployeeJobModel::class;
    }

    public function getEmployeeJobByid($id)
    {
        return $this->getByColumn($id, 'id_employee');
    }

    public function deleteEmployeeJob($id){
         $this->deleteById($id);
    }

}
