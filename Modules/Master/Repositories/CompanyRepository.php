<?php

namespace Modules\Master\Repositories;

use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;
use Modules\Master\Entities\CompaniesModel;

/**
 * Class CompanyRepository.
 */
class CompanyRepository extends BaseRepository
{
    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return CompaniesModel::class;
    }
}
