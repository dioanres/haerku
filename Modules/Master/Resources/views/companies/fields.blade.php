<fieldset>
    <div class="row">

        <!-- <input type="text" name="employee_info" id="employee_info" class="d-none"> -->
        <div class="col-sm-12">
            {!! Form::label('company_name', 'Company Name:') !!}  
            {!! Form::text('company_name', null, ['class' => 'form-control']) !!}

            {!! Form::label('address', 'Address:') !!}
            {!! Form::textarea('address', null, ['class' => 'form-control']) !!} 

            {!! Form::label('total_employee', 'Total Employee:') !!}
            {!! Form::number('total_employee', null, ['class' => 'form-control']) !!}

            {!! Form::label('no_telp', 'No Telp:') !!}
            {!! Form::text('no_telp', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</fieldset>