@extends('layouts.app', ['title' => __('Division')])

@section('content')
<div class="py-7">
    <div class="container-fluids">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">Division Form</h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 mt-3">
                        <form id="form-tambah-division" method="POST" enctype="multipart/form-data" action="{{route('master.division.store')}}">
                            {{ csrf_field() }}
                            <fieldset> 
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group" >
                                            <label for="nik" class="control-label">Company</label>
                                            <select class="form-control selectto" name="company_id" id="adm_empl_gender" required="1" aria-required="true">
                                                <option value="">Silahkan Pilih</option>
                                                <@foreach ($company as $company)
                                                    <option value="{{$company->id_company}}">{{$company->company_name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                            @include('master::division.fields-division')
                            <div class="container">
                                <div class="row">
                                    <div class="col-sm-11">
                                        <input class="btn btn-success" type="submit" id="save-division" name="SIMPAN" style="float: right;">
                                    </div>
                                    <div class="col-sm-1">
                                        <a href="{{ URL::previous() }}" type="button" class="btn btn-primary" style="float: right;">Back</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    
                    <div class="card-footer py-4">
                        <nav class="d-flex justify-content-end" aria-label="...">
                            
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    
</script>
@stop
