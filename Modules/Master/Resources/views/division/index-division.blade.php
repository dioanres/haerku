@extends('layouts.app', ['title' => __('Division')])

@section('content')
    <div class="py-7">
        <div class="container-fluids">
            <div class="row">
                <div class="col">
                    <div class="card shadow">
                        <div class="card-body">
                            <div class="tab-content">
                                <div class="col-12 active tab-pane fade show" role="tabpanel" aria-labelledby="division-form-tab" id="division-form">
                                    <form action="{{route('master.division.search')}}" method="GET">
                                        <div class="row align-items-center form-group col-sm-12" >
                                        {{ csrf_field() }}
                                            <div class="col-6">
                                                <label for="nik" class="control-label">List of Company</label>
                                                <select class="form-control selectto" name="companyId" id="adm_empl_gender" required="1" aria-required="true">
                                                    <option value="">Silahkan Pilih</option>
                                                    @foreach ($company as $company)
                                                    <option value="{{$company->id_company}}">{{$company->company_name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-4 text-left">
                                                <!-- <a class="btn btn-sm btn-primary" id="btn-find-division">Find Division</a> -->
                                                <input type="submit" value="Find Division" class="btn btn-sm btn-primary" >
                                            </div>
                                        </div>
                                    </form>
                                    <div class="card-header border-0">
                                        <div class="row align-items-center">
                                            <div class="col-8">
                                                <h3 class="mb-0">Division List</h3>
                                            </div>
                                            <div class="col-4 text-right">
                                                <a href="{{route('division.create')}}" class="btn btn-sm btn-primary">Add Division</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered table-hover dt-responsive nowrap" width="100%" id="table-division">
                                                <thead class="primary">
                                                    <tr>
                                                        <th>Division Description</th>
                                                        <th>Status</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody id='tbody-division'>
                                                    @forelse($division as $division)
                                                        <tr>
                                                            <td>{{ $division->division_desc }}</td>
                                                            @if(is_null($division->deleted_at))
                                                                <td>Active</td>
                                                            @else
                                                                <td>Non Active</td>
                                                            @endif
                                                            <td><a class="btn btn-sm btn-primary" id="" href="{{url('master/department/search/'.$division->division_id)}}">Department</a><a href="{{route('master.division.edit',['id' => $division->division_id])}}" class="btn btn-sm btn-success" id="">Edit</a><a href="{{route('master.division.delete',['id' => $division->division_id])}}" class="btn btn-sm btn-warning" id="">Delete</a></td>
                                                        </tr>
                                                        @empty
                                                        <tr>
                                                            <td></td>
                                                        </tr>
                                                    @endforelse
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer py-4">
                            <nav class="d-flex justify-content-end" aria-label="...">
                                
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<script type="text/javascript">
    var table_division = $('#table-division').DataTable({
        responsive: true,
    });
    $(document).ready(function(){
        table_division.columns.adjust().responsive.recalc();
    });
</script>
@stop