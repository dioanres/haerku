
            {{ FormHelper::form('text', 'Division Description', 'division_desc', ['required']) }}

            <div class="form-group">
                <label class="control-label" for="custom_query">Status</label>
                <div>
                    <div class="custom-control custom-radio custom-control-inline">
                      <input type="radio" id="division-status1" name="division_status" class="custom-control-input">
                      <label class="custom-control-label" for="division-status1">Active</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline">
                      <input type="radio" id="division-status2" name="division_status" class="custom-control-input">
                      <label class="custom-control-label" for="division-status2">Non Active</label>
                    </div>
                    <!-- <label class="custom-toggle">
                        <input type="checkbox" name="division_status" checked>
                        <span class="custom-toggle-slider rounded-circle" data-label-off="No" data-label-on="Yes"></span>
                    </label> -->
                </div>
            </div>
        </div>        
    </div>
</fieldset>