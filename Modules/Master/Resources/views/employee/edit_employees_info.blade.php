@extends('layouts.app', ['title' => __('input Data Employee')])

@section('content')
<div class="py-7">
    <div class="container-fluids">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="col-12 mt-3">
                          {!! Form::model($employees, ['route' => ['master.employees.update', collect($employees)->first() ], 'method' => 'patch','files' => true]) !!}
                            <p>Employee Info :</p>
                            <hr>
                           {{ csrf_field() }}
                             @include('master::employee.fields_employees_info')                            
                <input class="btn btn-success fixed-save" type="submit" id="save-data" value="SIMPAN">
            </form>
        </div>
        <div class="card-footer py-4">
            <nav class="d-flex justify-content-end" aria-label="...">

            </nav>
        </div>
    </div>
</div>
</div>
</div>
</div>
@stop
