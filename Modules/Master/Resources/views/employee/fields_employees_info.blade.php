<fieldset>
  <div class="accordion" id="accordionExample">
    <div class="card">
      <div class="card-header" id="headingOne" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
        <h5 class="mb-0">Employee Info</h5>
      </div>
      <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
        <div class="row">
          <div class="col-sm-6">
           {{ FormHelper::form('number', 'NIK', 'nik', ['required']) }}
           {{ FormHelper::form('text', 'Name ', 'fullname', ['required']) }}
           <div class="form-group" >
            {!! Form::label('id_company', 'Company') !!}  
            {!! Form::select('id_company[]', $comp, null, ['class' => 'form-control selectto', 'placeholder'=>''])!!}
          </div>
          <div class="form-group" >
            {!! Form::label('division_id', 'Division') !!}  
            {!! Form::select('division_id[]', $division, null, ['class' => 'form-control selectto', 'placeholder'=>''])!!}
          </div>
          <div class="form-group" >
            {!! Form::label('dept_id', 'Department') !!}  
            {!! Form::select('dept_id[]', $departement, null, ['class' => 'form-control selectto', 'placeholder'=>''])!!}
          </div>
          <div class="form-group" >
            <label for="nik" class="control-label">Department Head</label>
            <select class="form-control selectto" name="empl_dep_head" id="empl_dep_head">
             <!-- List Jenis Identitas dari DB -->
           </select>
         </div>
       </div>

       <div class="col-sm-6">
        <div class="form-group" >
          {!! Form::label('id_job', 'Job Position') !!}  
          {!! Form::select('id_job[]', $job, null, ['class' => 'form-control selectto', 'placeholder'=>''])!!}
        </div>
        <div class="form-group" >
          {!! Form::label('job_level_id', 'Job Level') !!}  
          {!! Form::select('job_level_id[]', $job_levels, null, ['class' => 'form-control selectto', 'placeholder'=>''])!!}
        </div>
        <div class="form-group" >
          {!! Form::label('empl_status_id', 'Employement Status') !!}  
          {!! Form::select('empl_status_id[]', $employee_status, null, ['class' => 'form-control selectto', 'placeholder'=>''])!!}
        </div>
        {{ FormHelper::form('datepicker', 'Join Date', 'join_date', ['required']) }}
        {{ FormHelper::form('datepicker', 'End Probation Date', 'end_date', ['required']) }}
        {{ FormHelper::form('text', 'Length Of Service', 'empl_length_service', ['readonly','required']) }}                                       
      </div>
    </div>
  </div>
</div>
<div class="card">
  <div class="card-header" id="headingTwo" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
    <h5 class="mb-0">Personal Info</h5>
  </div>
  <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
    <div class="row">
      <div class="col-sm-6">
        <div class="form-group" >
          {!! Form::label('identity_type_id', 'Identity Type') !!}  
          {!! Form::select('identity_type_id[]', $identity_type, null, ['class' => 'form-control selectto', 'placeholder'=>''])!!}
        </div>
        {{ FormHelper::form('text', 'NO ID', 'empl_id_no', ['required']) }}
        <div class="form-group" >
          <label for="nik" class="control-label">Gender</label>
          <select class="form-control selectto" name="gender" id="gender" required="1" aria-required="true">
            <option value="">Pilih Karyawan</option>
            <option value="1">Male</option>
            <option value="2">Female</option>
          </select>
        </div>
        <div class="form-group" >
          {!! Form::label('marital_status_id', 'Marital Status') !!}  
          {!! Form::select('marital_status_id[]', $marital_status, null, ['class' => 'form-control selectto', 'placeholder'=>''])!!}
        </div>
      </div>
      <div class="col-sm-6">
       {{ FormHelper::form('text', 'Place of Birth ', 'place_of_birth', ['required']) }}
       {{ FormHelper::form('datepicker', 'Birth Date', 'date_of_birth', ['required']) }}
       <div class="form-group" >
        {!! Form::label('religion_id', 'Religion') !!}  
        {!! Form::select('religion_id[]', $religion, null, ['class' => 'form-control selectto', 'placeholder'=>''])!!}
      </div>
      {{ FormHelper::form('text', 'Blood Type', 'blood_type') }}

    </div>
  </div>
</div>
</div>
<div class="card">
  <div class="card-header" id="headingThree" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
    <h5 class="mb-0">Contact Info</h5>
  </div>
  <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
   <div class="row">
    <div class="col-sm-6">
      {{ FormHelper::form('number', 'Mobile Phone 1', 'hp_no', ['required']) }}
      {{ FormHelper::form('number', 'Mobile Phone 2 ', 'hp_no_2', ['required']) }}
      {{ FormHelper::form('text', 'Email', 'email', ['required']) }}


    </div>
    <div class="col-sm-6">
     <div class="form-group">
      <label class="control-label" for="custom_query">Citizen ID Address</label>
      <div class="">
        <textarea class="form-control resize_vertical" id="id_address" name="id_address" rows="7"></textarea>
      </div>
    </div>
    <div class="form-group">
      <label class="control-label" for="custom_query">Residential Address</label>
      <div class="">
        <textarea class="form-control resize_vertical" id="current_address" name="current_address" rows="7"></textarea>
      </div>
    </div>

  </div>
</div>
</div>
</div>
<div class="card">
  <div class="card-header" id="headingFour" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
    <h5 class="mb-0">Emergency Contact</h5>
  </div>
  <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionExample">
    <div class="row">
      <div class="col-sm-6">
        {{ FormHelper::form('text', 'Emergency Contact Name', 'empl_ec_name', ['required']) }}
         {!! Form::label('relation_type_code', 'Emergency Contact Relationship') !!}  
            {!! Form::select('relation_type_code[]', array('1' => 'Ibu', '2' => 'Saudara'), null, ['class' => 'form-control selectto', 'placeholder'=>''])!!}
        {{ FormHelper::form('number', 'Emergency Contact Mobile Phone ', 'empl_ec_phone', ['required']) }}
      </div>
      <div class="col-sm-6">
        <div class="form-group">
          <label class="control-label" for="custom_query">Emergency Contact Address</label>
          <div class="">
            <textarea class="form-control resize_vertical" id="ec_address" name="ec_address" rows="7"></textarea>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>
<div class="card">
  <div class="card-header" id="headingFive" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
    <h5 class="mb-0">Salary</h5>
  </div>
  <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordionExample">
    <div class="row">
      <div class="col-sm-6">
        {{ FormHelper::form('text', 'Basic Salary', 'empl_salary', ['required']) }}
        <div class="form-group" >
          <label for="nik" class="control-label">Bank Name</label>
          <select class="form-control selectto" name="empl_bank_name" id="empl_bank_name" required="1" aria-required="">
            <option value="">Pilih Karyawan</option>
            <option value="1">Male</option>
            <option value="2">Female</option>
          </select>
        </div>
        {{ FormHelper::form('number', 'Rekening No ', 'empl_bank_no', ['required']) }}
        {{ FormHelper::form('text', 'Name On Rekening ', 'empl_name_of_bank', ['required']) }}
      </div>
    </div>
  </div>
</div>
<div class="card">
  <div class="card-header" id="headingSix" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
    <h5 class="mb-0">Tax</h5>
  </div>
  <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordionExample">
    <div class="row">
      <div class="col-sm-6">
        <div class="custom-control custom-radio mb-3">
          <input name="custom-radio-1" class="custom-control-input" id="customRadio1" type="radio">
          <label class="custom-control-label" for="customRadio5">Gross (with Tax)</label>
        </div>  
        <div class="custom-control custom-radio mb-3">
          <input name="custom-radio-2" class="custom-control-input" id="customRadio2" type="radio">
          <label class="custom-control-label" for="customRadio5">Gross Up</label>
        </div>  
        <div class="custom-control custom-radio mb-3">
          <input name="custom-radio-3" class="custom-control-input" id="customRadio3" type="radio">
          <label class="custom-control-label" for="customRadio5">Netto (without Tax)</label>
        </div>    
        {{ FormHelper::form('number', 'NPWP No ', 'empl_npwp_no', ['required']) }}
        <div class="form-group" >
          <label for="nik" class="control-label">PTKP Status</label>
          <select class="form-control selectto" name="empl_ptkp_status" id="empl_ptkp_status" required="1" aria-required="">
            <option value="">Pilih PTKP Status</option>
            <option value="1">Male</option>
            <option value="2">Female</option>
          </select>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="card">
  <div class="card-header" id="headingSeven" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
    <h5 class="mb-0">Benefits</h5>
  </div>
  <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordionExample">
    <div class="row">
      <div class="col-sm-6">
       {{ FormHelper::form('text', 'BPJS K ', 'empl_name_of_bank', ['required']) }}
       {{ FormHelper::form('text', 'BPJS TK', 'empl_name_of_bank', ['required']) }}
       <div class="row">      
        <div class="col-sm-5">  
         <p>Allowance</p>   
         <div class="custom-control custom-checkbox mb-3">
          <input class="custom-control-input" id="insentif_hadir" type="checkbox">
          <label class="custom-control-label" for="insentif_hadir">Insentif Hadir</label>
        </div>
        <div class="custom-control custom-checkbox mb-3">
          <input class="custom-control-input" id="reimburse" type="checkbox">
          <label class="custom-control-label" for="reimburse">Reimburse Kesehatan</label>
        </div>
        <div class="custom-control custom-checkbox mb-3">
          <input class="custom-control-input" id="overtime" type="checkbox">
          <label class="custom-control-label" for="overtime">Lembur</label>
        </div>
        <div class="custom-control custom-checkbox mb-3">
          <input class="custom-control-input" id="perjalanan_dinas" type="checkbox">
          <label class="custom-control-label" for="perjalanan_dinas">Perjalanan Dinas</label>
        </div>
        <div class="custom-control custom-checkbox mb-3">
          <input class="custom-control-input" id="tunjangan" type="checkbox">
          <label class="custom-control-label" for="tunjangan">Tunjangan Jabatan</label>
        </div>
      </div>
      <div class="col-sm-5">
        <div class="custom-control custom-checkbox mb-3">
          <input class="custom-control-input" id="tunjangan" type="checkbox">
          <label class="custom-control-label" for="tunjangan">Tunjangan Rumah</label>
        </div>
        <div class="custom-control custom-checkbox mb-3">
          <input class="custom-control-input" id="tunjangan" type="checkbox">
          <label class="custom-control-label" for="tunjangan">Tunjangan Transportasi</label>
        </div>
        <div class="custom-control custom-checkbox mb-3">
          <input class="custom-control-input" id="tunjangan" type="checkbox">
          <label class="custom-control-label" for="tunjangan">Uang Makan</label>
        </div>
        <div class="custom-control custom-checkbox mb-3">
          <input class="custom-control-input" id="tunjangan" type="checkbox">
          <label class="custom-control-label" for="tunjangan">dst</label>
        </div>
      </div>
    </div>
  </div>
  <div class="col-sm-6">
    <p>Benefits</p>
    <div class="custom-control custom-checkbox mb-3">
      <input class="custom-control-input" id="tunjangan" type="checkbox">
      <label class="custom-control-label" for="tunjangan">BPJS Kesehatan</label>
    </div>
    <div class="custom-control custom-checkbox mb-3">
      <input class="custom-control-input" id="tunjangan" type="checkbox">
      <label class="custom-control-label" for="tunjangan">BPJS Ketenagakerjaan JHT</label>
    </div>  
    <div class="custom-control custom-checkbox mb-3">
      <input class="custom-control-input" id="tunjangan" type="checkbox">
      <label class="custom-control-label" for="tunjangan">BPJS Ketenagakerjaan JKK</label>
    </div>
    <div class="custom-control custom-checkbox mb-3">
      <input class="custom-control-input" id="tunjangan" type="checkbox">
      <label class="custom-control-label" for="tunjangan">BPJS Ketenagakerjaan JP</label>
    </div>
    <div class="custom-control custom-checkbox mb-3">
      <input class="custom-control-input" id="tunjangan" type="checkbox">
      <label class="custom-control-label" for="tunjangan">Asuransi Lain</label>
    </div>
  </div>
</div>
</div>
</div>
</div>
</fieldset>