@extends('layouts.app', ['title' => __('Department')])

@section('content')
<div class="py-7">
    <div class="container-fluids">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    {!! Form::model($data, ['route' => ['master.department.store', collect($data)->first() ], 'method' => 'patch','files' => true]) !!}
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">Department Form</h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 mt-3">
                        <form id="form-tambah-department" method="POST" enctype="multipart/form-data" action="#">
                            {{ csrf_field() }}
                            @include('master::department.fields-department')
                            <input class="btn btn-success fixed-save" type="submit" id="save-division" name="SIMPAN">
                            
                        </form>
                    </div>
                    
                    <div class="card-footer py-4">
                        <nav class="d-flex justify-content-end" aria-label="...">
                            
                        </nav>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    
</script>
@stop
