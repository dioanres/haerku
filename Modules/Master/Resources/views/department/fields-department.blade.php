
<fieldset> 
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group" >
                <label for="nik" class="control-label">Company</label>
            </div>
            {{ FormHelper::form('text', 'division Description', 'division_desc', ['required']) }}
            {{ FormHelper::form('text', 'department Description', 'dept_desc', ['required']) }}

            <div class="form-group">
                <label class="control-label" for="custom_query">Status</label>
                <div>
                    <div class="custom-control custom-radio custom-control-inline">
                      <input type="radio" id="department-status1" name="department_status" class="custom-control-input">
                      <label class="custom-control-label" for="department-status1">Active</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline">
                      <input type="radio" id="department-status2" name="department_status" class="custom-control-input">
                      <label class="custom-control-label" for="department-status2">Non Active</label>
                    </div>
                    <!-- <label class="custom-toggle">
                        <input type="checkbox" name="division_status" checked>
                        <span class="custom-toggle-slider rounded-circle" data-label-off="No" data-label-on="Yes"></span>
                    </label> -->
                </div>
            </div>
        </div>
    </div>
</fieldset>

