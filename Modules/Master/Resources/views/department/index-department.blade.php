@extends('layouts.app', ['title' => __('Department')])

@section('content')
    <div class="py-7">
        <div class="container-fluids">
            <div class="row">
                <div class="col">
                    <div class="card shadow">
                        <div class="card-body">
                            <div class="tab-content">
                                <div class="col-12 active mt-3 tab-pane fade show" role="tabpanel" aria-labelledby="department-form-tab" id="department-form">
                                    <div class="card-header border-0">
                                        <div class="row align-items-center">
                                            <div class="col-8">
                                                <h3 class="mb-0">Department List</h3>
                                            </div>
                                            <div class="col-4 text-right">
                                                <a href="{{route('department.create',['id' => $department[0]->division_id])}}" class="btn btn-sm btn-primary">Add Department</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered table-hover dt-responsive nowrap" width="100%" id="table-department">
                                                <thead class="primary">
                                                    <tr>
                                                        <th>Department Description</th>
                                                        <th>Status</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody> 
                                                     @forelse($department as $department)
                                                        <tr>
                                                            <td>{{ $department->dept_desc }}</td>
                                                            @if(is_null($department->deleted_at))
                                                                <td>Active</td>
                                                            @else
                                                                <td>Non Active</td>
                                                            @endif
                                                            <td><a href="{{route('master.department.edit',['id' => $department->dept_id])}}" class="btn btn-sm btn-success" id="">Edit</a><a href="{{route('master.department.delete',['id' => $department->dept_id])}}" class="btn btn-sm btn-primary" id="#">Delete</a></td>
                                                        </tr>
                                                        @empty
                                                        <tr>
                                                            <td></td>
                                                        </tr>
                                                    @endforelse
                                                </tbody>
                                            </table>
                                        </div>
                                    </div> 
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <a href="{{ URL::previous() }}" type="button" class="btn btn-primary" style="float: right;">Back</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>    
                            </div>
                        </div>
                        <div class="card-footer py-4">
                            <nav class="d-flex justify-content-end" aria-label="...">
                                
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<script type="text/javascript">

    var table_department = $('#table-department').DataTable({
        responsive: true,
    });
    $(document).ready(function(){
        table_department.columns.adjust().responsive.recalc();
    });
</script>
@stop