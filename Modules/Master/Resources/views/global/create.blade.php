@extends('layouts.app', ['title' => __('Global')])

@section('content')
<div class="py-7">
    <div class="container-fluids">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">Division Form</h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 mt-3">
                        <form id="form-tambah-global" method="POST" enctype="multipart/form-data" action="{{route('global.store')}}">
                            {{ csrf_field() }}
                            <!-- @include('master::global.fields-global') -->
                            <fieldset>
                                <div class="row">
                                    <div class="col-sm-6">

                                        <div class="form-group" >
                                            <label for="nik" class="control-label">Company</label>
                                            <select class="form-control selectto" name="id_company" id="id_company" required="1" aria-required="true">
                                             <option value="">Pilih Company</option>
                                                @foreach ($company as $company)
                                                <option value="{{ $company->id_company }}">
                                                    {{ $company->company_name }}
                                                </option>
                                                @endforeach
                                            </select>
                                        </div>


                                        {{ FormHelper::form('text', 'Parameter Type', 'param_type', ['required']) }}

                                        <div class="form-group col-sm-12">
                                            <label class="control-label " for="custom_query">Status</label>
                                            <div>
                                                <div class="radio custom-control custom-radio mb-3 custom-control-inline">
                                                    <input type="radio" id="radio1" name="status" value="0" class="custom-control-input" >
                                                    <label class="custom-control-label" for="radio1">Active</label>
                                                </div>
                                                <div class="radio custom-control custom-radio mb-3 custom-control-inline">
                                                    <input type="radio" id="radio2" name="status" value="1" class="custom-control-input" >
                                                    <label class="custom-control-label" for="radio2">Non Active</label>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <hr />
                                    <!-- <div class="table-responsive" style="margin-left: 10px;margin-right: 10px;">
                                        <table class="table table-striped table-bordered table-hover dt-responsive nowrap" width="100%" id="table-global-param">
                                            <thead class="primary">
                                                <tr>
                                                    <th>Parameter Id</th>
                                                    <th>Parameter Description</th>
                                                    <th>Aksi</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                            
                                </div> -->
                            </fieldset>
                            
                            <input class="btn btn-success fixed-save" type="submit" id="save-global" name="SIMPAN">
                            
                        </form>
                    </div>
                    
                    <div class="card-footer py-4">
                        <nav class="d-flex justify-content-end" aria-label="...">
                            
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    
</script>
@stop
