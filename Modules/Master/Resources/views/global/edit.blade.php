@extends('layouts.app', ['title' => __('Edit Global')])

@section('content')
<div class="py-7">
    <div class="container-fluids">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">Global Form</h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 mt-3">
                            <fieldset>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <form id="form-tambah-global" method="POST" enctype="multipart/form-data" action="{{route('global.update', ['id' => $param->global_id]) }}">
                                            {{ csrf_field() }}
                                            <div class="form-group" >
                                                <label for="nik" class="control-label">Company</label>
                                                <select class="form-control selectto" name="id_company" id="id_company" required="1" aria-required="true" value="{{ $param->id_company }}">
                                                 <option value="">Pilih Company</option>
                                                    @foreach ($company as $company)
                                                    <option value="{{ $company->id_company }}">
                                                        {{ $company->company_name }}
                                                    </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label" for="$id">Param Type</label>
                                                <div class="input-group">
                                                    <input type="text" name="param_type" id="'param_type" class="form-control" required value="{{ $param->param_type }}" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label " for="custom_query">Status</label>
                                                <div>
                                                    <div class="radio custom-control custom-radio mb-3 custom-control-inline">
                                                        <input type="radio" id="radio1" name="status" value="0" class="custom-control-input" {{ ($param->status=="0")? "checked" : "" }} >
                                                        <label class="custom-control-label" for="radio1">Active</label>
                                                    </div>
                                                    <div class="radio custom-control custom-radio mb-3 custom-control-inline">
                                                        <input type="radio" id="radio2" name="status" value="1" class="custom-control-input" {{ ($param->status=="1")? "checked" : "" }} >
                                                        <label class="custom-control-label" for="radio2">Non Active</label>
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                                    <hr />
                                    <div class="table-responsive" style="margin-left: 10px;margin-right: 10px;">
                                        <div class="col-12 text-right">
                                            <button type="button" class="btn btn-sm btn-primary" id="btn-add-new" >Add Parameter</button>
                                        </div>
                                        <br />
                                        <table class="table table-striped table-bordered table-hover dt-responsive nowrap" width="100%" id="table-global-param">
                                            <thead class="primary">
                                                <tr>
                                                    <th>Parameter Id</th>
                                                    <th>Parameter Description</th>
                                                    <th>Status</th>
                                                    <th>Aksi</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($list as $global)
                                                <tr>
                                                    <td>{{ $global->param_id }}</td>
                                                    <td>{{ $global->param_desc }}</td>
                                                    @if($global->status)
                                                        <td>Active</td>
                                                        @else
                                                        <td>Non Active</td>
                                                    @endif
                                                    <td>
                                                        <button type="button" class="btn btn-sm btn-warning mb-3" onclick="edit_line(this, {{ $global->global_dtl_id }})">Edit</button>
                                                        <a type="button" class="btn btn-sm btn-danger mb-3" href="{{route('global.deleteDtl',['id' => $global->global_dtl_id])}}">Delete</a>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                            
                                </div>
                            </fieldset>
                            
                            <input class="btn btn-success fixed-save" type="submit" id="save-global" name="SIMPAN">
                            
                        </form>
                    </div>
                    
                    <div class="card-footer py-4">
                        <nav class="d-flex justify-content-end" aria-label="...">
                            
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modal-default" aria-hidden="true" id="modal-detail">
    <div class="modal-dialog modal- modal-dialog-centered modal-" role="document">
        <div class="modal-content">
            <form id="form-tambah-global-dtl" method="POST" enctype="multipart/form-data" action="{{ route('global.storeDtl') }}">
                {{ csrf_field() }}
                <div class="modal-header">
                    <h3 class="modal-title" id="modal-title-default">Parameter {{ $param->param_type }}</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                
                <div class="modal-body">
                   {{ FormHelper::form('text', 'Parameter Id', 'param_id', ['required']) }} 
                   {{ FormHelper::form('text', 'Parameter Description', 'param_desc', ['required']) }} 
                    <input type="hidden" name="global_id" class="form-control" required value="{{ $param->global_id }}" />
                    <input type="hidden" name="global_dtl_id" class="form-control" />
                    <div class="form-group">
                        <label class="control-label " for="custom_query">Status</label>
                        <div>
                            <div class="radio custom-control custom-radio mb-3 custom-control-inline">
                                <input type="radio" id="radio-dtl1" name="status" value="0" class="custom-control-input">
                                <label class="custom-control-label" for="radio-dtl1">Active</label>
                            </div>
                            <div class="radio custom-control custom-radio mb-3 custom-control-inline">
                                <input type="radio" id="radio-dtl2" name="status" value="1" class="custom-control-input" >
                                <label class="custom-control-label" for="radio-dtl2">Non Active</label>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="modal-footer">
                    <input class="btn btn-primary" type="submit" id="save-global-dtl" name="simpan_dt">
                    <button type="button" class="btn btn-link  ml-auto" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('#btn-add-new').click(function(){
        $('#modal-detail').modal('show');
        $('[name=param_id], [name=param_desc], [name=global_dtl_id]').val('');
        $('#radio-dtl2, #radio-dtl1').attr('checked', false);
    });
    var tbl_param_dtl = $('#table-global-param').DataTable({
        responsive: true,
        lengthChange: false
        // columns: [
        //     { "name": "param_id" },
        //     { "name": "param_desc" },
        //     { "name": "status" }
        // ]
    });
    function edit_line(element, id){
        var data = tbl_param_dtl.rows($(element).parents('tr')).data()[0];
        console.log(data)
        $('#modal-detail').modal('show');
        $('[name=param_id]').val(data[0]);
        $('[name=param_desc]').val(data[1]);
        $('[name=global_dtl_id]').val(id);
        if(data[2] == 'Non Active'){
            $('#radio-dtl2').attr('checked', true);
        } else {
            $('#radio-dtl1').attr('checked', true);
        }
    }
</script>
@stop