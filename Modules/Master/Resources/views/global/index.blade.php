@extends('layouts.app', ['title' => __('Global')])

@section('content')
	<div class="py-7">
		<div class="container-fluids">
			<div class="row">
	            <div class="col">
	                <div class="card shadow">
	                    <div class="card-header border-0">
	                        <div class="row align-items-center">
	                            <div class="col-8">
	                                <h3 class="mb-0">Global</h3>
	                            </div>
	                            <div class="col-4 text-right">
	                                <a href="{{route('global.create')}}" class="btn btn-sm btn-primary">Add Global</a>
	                            </div>
	                        </div>
	                    </div>
	                    <div class="col-sm-12">
	                    	<div class="table-responsive">
		                        <table class="table table-striped table-bordered table-hover dt-responsive nowrap" width="100%" id="table-global">
		                            <thead class="primary">
		                                <tr>
		                                    <th>Parameter Type</th>
		                                    <th>Status</th>
		                                    <th>Aksi</th>
		                                </tr>
		                            </thead>
		                            <tbody>
		                                 @foreach ($list as $global)
		                                <tr>
                                            <td>{{ $global->param_type }}</td>
                                            @if($global->status)
												<td>Active</td>
												@else
												<td>Non Active</td>
											@endif
                                            <td>
                                                <a class="btn btn-sm btn-warning" href="{{route('global.edit',['id' => $global->global_id])}}">Edit</a>
                                                <a class="btn btn-sm btn-danger" href="{{route('global.delete',['id' => $global->global_id])}}">Delete</a>
                                            </td>
                                        </tr>
                                        @endforeach
		                            </tbody>
		                        </table>
		                    </div>
	                    </div>
	                    <div class="card-footer py-4">
	                        <nav class="d-flex justify-content-end" aria-label="...">
	                            
	                        </nav>
	                    </div>
	                </div>
	            </div>
	        </div>
		</div>
	</div>

@stop