@extends('layouts.app', ['title' => __('Input Data Job')])

@section('content')
<div class="py-7">
    <div class="container-fluids">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">Job Info</h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 mt-3">
                        <form id="form-tambah-job" method="POST" enctype="multipart/form-data" action="
                        {{route('master.job.store')}}">                    
                        <fieldset>
                            <div class="row">
                                <div class="col-sm-6">
                                    <hr>
                                    {{ csrf_field() }}
                                     @include('master::job.fields-job')
                                </div>   
                            </div>
                        </fieldset>                            
                        <input class="btn btn-success fixed-save" type="submit" id="save-job" name="SIMPAN">

                    </form>
                </div>

                <div class="card-footer py-4">
                    <nav class="d-flex justify-content-end" aria-label="...">

                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<script type="text/javascript">

</script>
@stop
