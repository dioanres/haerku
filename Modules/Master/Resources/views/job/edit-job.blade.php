@extends('layouts.app', ['title' => __('Edit Data Job')])

@section('content')
<div class="py-7">
    <div class="container-fluids">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="col-12 mt-3">
                        {!! Form::model($job, ['route' => ['master.job.update', collect($job)->first() ], 'method' => 'patch','files' => true]) !!}
                        <p>Job Info :</p>
                        <hr>
                        {{ csrf_field() }}
                         @include('master::job.fields-job')
                    <div class="card-footer py-4">
                        <input class="btn btn-success fixed-save" type="submit" id="save-data" value="SIMPAN">
                        <nav class="d-flex justify-content-end" aria-label="...">

                        </nav>
                    </div>
                    {!! Form::close() !!}
                </div>                    
            </div>
        </div>
    </div>
</div>
</div>
@stop
