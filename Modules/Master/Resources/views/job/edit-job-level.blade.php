@extends('layouts.app', ['title' => __('Edit Data Job')])

@section('content')
<div class="py-7">
    <div class="container-fluids">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="col-12 mt-3">

                        {!! Form::model($job_levels, ['route' => ['master.job.level.update', collect($job_levels)->first() ], 'method' => 'patch','files' => true]) !!}
                        <p>Job Level Info :</p>
                        <hr>
                        {{ FormHelper::form('text', 'Job Level Description', 'job_level_desc', ['required']) }} 
                        <div class="form-group col-sm-12">
                            <label class="control-label " for="custom_query">Status</label>
                            <div>
                                <div class="radio custom-control custom-radio mb-3 custom-control-inline">
                                    {!! Form::radio('status', 1, $status==1) !!}
                                    {!! Form::label('radio1', 'Active') !!}

                                </div>
                                <div class="radio custom-control custom-radio mb-3 custom-control-inline">
                                    {!! Form::radio('status', 0, $status==0) !!}
                                    {!! Form::label('radio2', 'Non Active') !!}
                                </div>
                            </div>
                        </div>   
                        <div class="card-footer py-4">
                            <input class="btn btn-success fixed-save" type="submit" id="save-data" value="SIMPAN">
                            <nav class="d-flex justify-content-end" aria-label="...">

                            </nav>
                        </div>
                        {!! Form::close() !!}
                    </div>                    
                </div>
            </div>
        </div>
    </div>
</div>
@stop
