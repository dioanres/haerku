  <fieldset>
  	<div class="row">
  		<div class="col-sm-12">
  			{!! Form::label('job_desc', 'Job Description:') !!}  
  			{!! Form::text('job_desc', null, ['class' => 'form-control']) !!}
  			{!! Form::label('job_level_id', 'Job Level:') !!}  
  			{!! Form::select('job_level_id[]', $job_levels, $curr_job_level_id, ['class' => 'form-control selectto', 'placeholder'=>''])!!}
  			{!! Form::label('dept_id', 'Department:') !!}  
  			{!!Form::select('dept_id[]', $dept, $curr_dept_id, ['class' => 'form-control selectto', 'placeholder'=>''])!!}
  		</div>
  		<div class="form-group col-sm-12">
  			<label class="control-label " for="custom_query">Status</label>
  			<div>
  				<div class="radio custom-control custom-radio mb-3 custom-control-inline">
  					{!! Form::radio('status', 1, $status==1) !!}
  					{!! Form::label('radio1', 'Active') !!}

  				</div>
  				<div class="radio custom-control custom-radio mb-3 custom-control-inline">
  					{!! Form::radio('status', 0, $status==0) !!}
  					{!! Form::label('radio2', 'Non Active') !!}
  				</div>
  			</div>
  		</div>

  	</div>
  </fieldset>