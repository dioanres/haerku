@extends('layouts.app', ['title' => __('Job Level')])

@section('content')
<div class="py-7">
    <div class="container-fluids">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">Job Level Form</h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 mt-3">
                        <form id="form-tambah-job-level" method="POST" enctype="multipart/form-data" action="
                        {{route('master.job.level.store')}}">
                        {{ csrf_field() }}
                        <fieldset>
                            <div class="row">
                                <div class="col-sm-6">
                                    {{ FormHelper::form('text', 'Job Level Description', 'job_level_desc', ['required']) }}  
                                    {!! Form::hidden('created_by', 'RENI', ['class' => 'form-control']) !!}            
                                    <div class="form-group col-sm-12">
                                    <label class="control-label " for="custom_query">Status</label>
                                    <div>
                                        <div class="radio custom-control custom-radio mb-3 custom-control-inline">
                                            {!! Form::radio('status', 1, true) !!}
                                            {!! Form::label('radio1', 'Active') !!}

                                        </div>
                                        <div class="radio custom-control custom-radio mb-3 custom-control-inline">
                                            {!! Form::radio('status', 0, false) !!}
                                            {!! Form::label('radio2', 'Non Active') !!}
                                        </div>
                                    </div>
                                </div>                             
                                </div>                         
                                
                            </div>
                        </fieldset>

                        <input class="btn btn-success fixed-save" type="submit" id="save-job-level" name="SIMPAN">

                    </form>
                </div>

                <div class="card-footer py-4">
                    <nav class="d-flex justify-content-end" aria-label="...">

                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<script type="text/javascript">

</script>
@stop
