@extends('layouts.app', ['title' => __('Jobs')])

@section('content')
<div class="py-7">
	<div class="container-fluids">
		<div class="row">
			<div class="col">
				<div class="card shadow">
					<div class="nav-wrapper col-12 mt-3">
						<ul class="nav nav-pills nav-fill flex-column flex-md-row" id="tabs-icons-text" role="tablist">	  
							<li class="nav-item">
								<a class="nav-link mb-sm-2 mb-md-0 active" id="jobs-form-tab" data-toggle="tab" href="#jobs-form" role="tab" aria-controls="jobs-form" aria-selected="true"><i class="ni ni-cloud-upload-96 mr-2"></i>Jobs</a>
							</li>                      
							<li class="nav-item">
								<a class="nav-link mb-sm-2 mb-md-0" id="job-level-form-tab" data-toggle="tab" href="#job-level-form" role="tab" aria-controls="job-level-form" aria-selected="true"><i class="ni ni-cloud-upload-96 mr-2"></i>Job Level</a>
							</li>	                          
						</ul>
					</div>
					<div class="tab-content">
						<div class="col-12 mt-3 tab-pane fade show active" role="tabpanel" aria-labelledby="jobs-form-tab" id="jobs-form">
							<div class="card-header border-0">
								<div class="row align-items-center">
									<div class="col-8">
										<h3 class="mb-0">Jobs List</h3>
									</div>
									<div class="col-4 text-right">
										<a href="{{route('master.job.create')}}" class="btn btn-sm btn-primary">Add Job</a>
									</div>
								</div>
							</div>
							<div class="col-sm-12">
								<div class="table-responsive">
									<table class="table table-striped table-bordered table-hover dt-responsive nowrap" width="100%" id="table-job">
										<thead class="primary">
											<tr>
												<th>Job ID</th>
												<th>Job Description</th>
												<th>Job Level Description</th>
												<th>Status</th>
												<td>Aksi</td>
											</tr>
										</thead>
										<tbody>
											@foreach ($job as $job)														
											<tr style="cursor: pointer;">
												<td>{{$job->id_job}}</td>
												<td>{{$job->job_desc}}</td>
												@if($job->job_level_id == null)
												<td></td>
												@else
												@foreach ($job_levels as $joblevel)
												@if ($joblevel->job_level_id == $job->job_level_id)
												<td>{{$joblevel->job_level_desc}}</td>
												@endif
												@endforeach
												@endif
												@if($job->status)
												<td>Active</td>
												@else
												<td>Non Active</td>
												@endif
												<td>
													<a class="btn btn-sm btn-warning" href="{{route('master.job.edit',['id' => $job->id_job])}}">Edit</a>
													<a class="btn btn-sm btn-danger" href="{{route('master.job.delete',['id' => $job->id_job])}}">Delete</a>
												</td>
											</tr>											
											@endforeach
										</tbody>
									</table>
								</div>
							</div>  
						</div>
						<div class="col-12 mt-3 tab-pane fade" role="tabpanel" aria-labelledby="job-level-form-tab" id="job-level-form">
							<div class="card-header border-0">
								<div class="row align-items-center">
									<div class="col-8">
										<h3 class="mb-0">Jobs Level List</h3>
									</div>
									<div class="col-4 text-right">
										<a href="{{route('master.job.level.create')}}" class="btn btn-sm btn-primary">Add Job Level</a>
									</div>
								</div>
							</div>

							<div class="col-sm-12">
								<div class="table-responsive">
									<table class="table table-striped table-bordered table-hover dt-responsive nowrap" width="100%" id="table-job-level">
										<thead class="primary">
											<tr>
												<th>Job Level ID</th>
												<th>Job Level Description</th>
												<th>Status</th>
												<td>Aksi</td>
											</tr>
										</thead>
										<tbody>
											@foreach ($job_levels as $job_levels)									
											<tr style="cursor: pointer;">
												<td>{{$job_levels->job_level_id}}</td>
												<td>{{$job_levels->job_level_desc}}</td>
												@if($job_levels->status)
												<td>Active</td>
												@else
												<td>Non Active</td>
												@endif
												<td>
													<a class="btn btn-sm btn-warning" href="{{route('master.job.level.edit',['id' => $job_levels->job_level_id])}}">Edit</a>
													<a class="btn btn-sm btn-danger" href="{{route('master.job.level.delete',['id' => $job_levels->job_level_id])}}">Delete</a>
												</td>
											</tr>
											@endforeach
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					
					<div class="card-footer py-4">
						<nav class="d-flex justify-content-end" aria-label="...">

						</nav>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	var table_job = $('#table-job').DataTable({
		responsive: true,
	});
	$(document).ready(function(){
		table_job.columns.adjust().responsive.recalc();
	});

	var table_job_level = $('#table-job-level').DataTable({
		responsive: true,
	});
	$(document).ready(function(){
		table_job_level.columns.adjust().responsive.recalc();
	});
</script>
@stop