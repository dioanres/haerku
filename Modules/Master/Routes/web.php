<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('master')->middleware('auth')->group(function() {
    Route::get('/', 'MasterController@index')->name('master');

    //DIVISION
    Route::prefix('division')->group(function () {
        Route::get('/', 'DivisionController@index')->name('division');
        Route::get('/create', ['as' => 'division.create', 'uses' => 'DivisionController@create']);
        Route::post('/store', 'DivisionController@store')->name('master.division.store');
        Route::get('/search', 'DivisionController@search')->name('master.division.search');
        Route::get('/{id}/edit', 'DivisionController@edit')->name('master.division.edit');
        Route::patch('/{id}/update', 'DivisionController@update')->name('master.division.update');
        Route::get('/{id}/delete', 'DivisionController@delete')->name('master.division.delete');
    }); 

    Route::prefix('department')->group(function () {
        Route::get('/', 'DepartmentController@index')->name('department');
        Route::get('/{id}/create', ['as' => 'department.create', 'uses' => 'DepartmentController@create']);
        Route::get('/search/{id}','DepartmentController@search')->name('master.department.search');
        Route::patch('/{id}/store', 'DepartmentController@store')->name('master.department.store');
        Route::get('/{id}/edit', 'DepartmentController@edit')->name('master.department.edit');
        Route::patch('/{id}/update', 'DepartmentController@update')->name('master.department.update');
        Route::get('/{id}/delete', 'DepartmentController@delete')->name('master.department.delete');
    }); 

    Route::prefix('companies')->group(function () {
        Route::get('/', 'CompaniesController@index')->name('master.companies');
        Route::get('/create', 'CompaniesController@create')->name('master.companies.create');
        Route::post('/store', 'CompaniesController@store')->name('master.companies.store');
        Route::get('/{id}/edit', 'CompaniesController@edit')->name('master.companies.edit');
        Route::patch('/{id}/update', 'CompaniesController@update')->name('master.companies.update');
        Route::put('/{id}/update', 'CompaniesController@update')->name('master.companies.update');
        //Route::get('/create', ['as' => 'division.create', 'uses' => 'DivisionController@create']);
    }); 

    Route::prefix('employees')->group(function () {
        Route::get('/','EmployeesController@index')->name('master.employees');
        Route::get('/create', 'EmployeesController@create')->name('master.employees.create');
        Route::post('/store', 'EmployeesController@store')->name('master.employees.store');
        Route::get('/{id}/edit', 'EmployeesController@edit')->name('master.employees.edit');
        Route::get('/{id}/delete', 'EmployeesController@destroy')->name('master.employees.delete');
        Route::patch('/{id}/update', 'EmployeesController@update')->name('master.employees.update');
    }); 

    Route::prefix('job')->group(function () {
        Route::get('/', 'JobController@index')->name('master.job');
        Route::get('/create', 'JobController@create')->name('master.job.create');
        Route::post('/store', 'JobController@store')->name('master.job.store');        
        Route::get('/{id}/edit', 'JobController@edit')->name('master.job.edit');
        Route::get('/{id}/delete', 'JobController@destroy')->name('master.job.delete');
        Route::patch('/{id}/update', 'JobController@update')->name('master.job.update');
    }); 

    Route::prefix('job/level')->group(function () {
        Route::get('/', 'JobLevelController@index')->name('master.job.level');
        Route::get('/create', 'JobLevelController@create')->name('master.job.level.create');
        Route::post('/store', 'JobLevelController@store')->name('master.job.level.store');
        Route::get('/{id}/edit', 'JobLevelController@edit')->name('master.job.level.edit');
        Route::get('/{id}/delete', 'JobLevelController@destroy')->name('master.job.level.delete');
        Route::patch('/{id}/update', 'JobLevelController@update')->name('master.job.level.update');  
    }); 
	
	Route::prefix('global')->group(function () {
        Route::get('/','GlobalController@index')->name('global');
        Route::get('/create','GlobalController@create')->name('global.create');
        Route::post('/store', 'GlobalController@store')->name('global.store');
        Route::get('/{id}/edit', 'GlobalController@edit')->name('global.edit');
        Route::post('/{id}/update', 'GlobalController@update')->name('global.update');
        Route::post('/storeDtl', 'GlobalController@storeDtl')->name('global.storeDtl');
        Route::get('/{id}/delete', 'GlobalController@destroy')->name('global.delete');
        Route::get('/{id}/deleteDtl', 'GlobalController@destroyDtl')->name('global.deleteDtl');
    });

});
