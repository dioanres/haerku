<?php

namespace Modules\Payroll\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use DB;
use Flash;
use Modules\Payroll\Models\{Payslip_Header, Payslip_Detail};
use Modules\Employees\Models\Employee;
use App\Models\{Mst_Payslip};

class PayrollController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $data['payrolls'] = Payslip_Header::all();
        return view('payroll::index', $data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        // dd(Employee::getKeyName());
        $data['employees'] = Employee::list();
        $data['payslips'] = Mst_Payslip::all();
        $data['payroll'] = '';
        $data['payroll_detail'] = '';
        return view('payroll::create', $data);
    }

    public function edit($id)
    {
        $data['employees'] = Employee::list();
        $data['payslips'] = Mst_Payslip::all();
        $data['payroll'] = Payslip_Header::find($id);
        $data['payroll_detail'] = Payslip_Detail::detail($id);

        return view('payroll::create', $data);
    }

    public function payroll_detail_insert($data, $payslip_id){
        if ($data != null) {
            foreach ($data as $key => $value) {
                $details = json_decode($value);

                $details->payslip_id = $payslip_id;
                $details->updated_by = 'tes';
                $payslip_detail[$key] = (array)$details;

                Payslip_Detail::updateOrCreate(['payslip_dtl_id' => $payslip_detail[$key]['payslip_dtl_id']], $payslip_detail[$key]);
            }
        }
        return '';
    }
    public function store(Request $request)
    {
        $request->merge(['start_date' => date("Y-m-d", strtotime($request->input('start_date')))]);
        $request->merge(['end_date' => date("Y-m-d", strtotime($request->input('end_date')))]);
        $request->merge(['created_by' => 'tes']);
        $request->merge(['updated_by' => 'tes']);
        $error = "";

        DB::beginTransaction();
        try {
            $payslip = Payslip_Header::updateOrCreate(['payslip_id' => $request->input('payslip_id')], $request->all());

            $error = $this->payroll_detail_insert($request->detail, $payslip->payslip_id);
            if ($error != "") {
                throw new Exception($error);
            }

            DB::commit();

            // Flash::success("Data berhasil disimpan.");
        } catch (\Exception $ex) {
            DB::rollback();
            return $ex->getMessage();
            // Flash::error($ex->getMessage());
        }

        return $payslip->id;
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('payroll::show');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
