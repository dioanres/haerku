<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::prefix('payroll')->middleware(['auth'])->group(function () {
    Route::get('/', 'PayrollController@index')->name('payroll');
    Route::get('/{payroll}/edit', ['as' => 'payroll.edit', 'uses' => 'PayrollController@edit']);
    Route::get('/create', ['as' => 'payroll.create', 'uses' => 'PayrollController@create']);
    Route::post('/store', ['as' => 'payroll.store', 'uses' => 'PayrollController@store']);
});