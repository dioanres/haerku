<?php

namespace Modules\Payroll\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use Session;
use DB;


class Payslip_Detail extends Model
{
    public $table = 'payslip_detail';

    public $fillable = [
        // 'payslip_dtl_id',
        'payslip_id',
        'payslip_code',
        'percent_fee',
        'amount_fee',
        'custom_query',
        'pay_by',
        'created_date',
        'created_by',
        'updated_date',
        'updated_by',
    ];

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'updated_date';
    protected $primaryKey = 'payslip_dtl_id';
    // public $incrementing = true;

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */

    /**
     * Validation rules
     *
     * @var array
     */

    public static function detail($id){
        return DB::select("SELECT pd.*, mp.payslip_desc
                from payslip_detail pd
                LEFT join mst_payslip mp on mp.payslip_code = pd.payslip_code
                WHERE pd.payslip_id = ?
            ;", [$id]);
    }
}
