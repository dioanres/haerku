<?php

namespace Modules\Payroll\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Master\Entities\EmployeesModel;
use Session;
use DB;


class Payslip_Header extends Model
{
    public $table = 'payslip_header';

    public $fillable = [
        'payslip_id',
        'start_date',
        'end_date',
        'nik',
        'gaji_pokok',
        'take_home_pay',
        'created_date',
        'created_by',
        'updated_date',
        'updated_by',
    ];

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'updated_date';
    protected $primaryKey = 'payslip_id';

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */

    /**
     * Validation rules
     *
     * @var array
     */

    public static function list()
    {
        return DB::select("SELECT ph.payslip_id, DATE_FORMAT(start_date, '%d %b %Y') start_date, DATE_FORMAT(end_date, '%d %b %Y') end_date, ph.nik, gaji_pokok, take_home_pay, e.fullname
            from payslip_header ph
            LEFT join employee e on e.nik = ph.nik
            ;");
    }

    public static function detail($id){
        return DB::select("SELECT ph.payslip_id, DATE_FORMAT(start_date, '%d %b %Y') start_date, DATE_FORMAT(end_date, '%d %b %Y') end_date, ph.nik, gaji_pokok, take_home_pay, e.fullname
            from payslip_header ph
            LEFT join employee e on e.nik = ph.nik
            where ph.payslip_id = ?
            ;", [$id]);
    }

    public function employee()
    {
        return $this->belongsTo(EmployeesModel::Class, 'id_employee', 'id');
    }
}
