@extends('layouts.app', ['title' => __('Payroll')])

@section('content')
<div class="py-7">
    <div class="container-fluids">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="col-12 mt-3">
                        <form id="form-tambah-payroll" method="POST" enctype="multipart/form-data" action="#">
                            {{ csrf_field() }}
                            <fieldset>
                                <div class="row">
                                    <input type="text" name="payslip_id" id="payslip_id" class="d-none">
                                    <div class="col-sm-6">
                                        <div class="form-group" >
                                            <label for="nik" class="control-label">Karyawan</label>
                                            <select class="form-control selectto" name="nik" id="nik" required="1" aria-required="true">
                                                <option value="">Pilih Karyawan</option>
                                                @foreach ($employees as $employee)
                                                <option value="{{ $employee->nik }}">
                                                    {{ $employee->fullname }}
                                                </option>
                                                @endforeach
                                            </select>
                                        </div>

                                        {{ FormHelper::form('datepicker', 'Start Date', 'start_date', ['required']) }}
                                        {{ FormHelper::form('datepicker', 'End Date', 'end_date', ['required']) }}
                                    </div>

                                    <div class="col-sm-6">
                                        {{ FormHelper::form('number', 'Gaji Pokok', 'gaji_pokok', ['readonly', 'required']) }}
                                        {{ FormHelper::form('number', 'Gaji Bersih', 'take_home_pay', ['readonly', 'required']) }}
                                    </div>
                                </div>
                            </fieldset>
                            <div class="row">
                                <div class="col-sm-12">
                                    <span class="btn btn-success btn-sm float-right" id="btn-tambah-detail">Tambah Komponen Gaji</span>
                                </div>
                            </div>
                            <div class="row" style="margin-top: 10px;">
                                <div class="col-sm-12">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered dt-responsive nowrap table-sm" width="100%" id="table-payroll">
                                            <thead class="primary">
                                                <tr>
                                                    <th>Komponen Gaji</th>
                                                    <th>Tipe</th>
                                                    <th>Persentase Gaji</th>
                                                    <th>Jumlah</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <input class="btn btn-success btn-sm fixed-save" type="submit" id="save-data" name="SIMPAN">

                        </form>
                    </div>

                    <div class="card-footer py-4">
                        <nav class="d-flex justify-content-end" aria-label="...">

                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-detail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Komponen Gaji</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="form-detail-payroll">
                <input type="text" name="payslip_dtl_id" id="payslip_dtl_id" class="d-none">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group" >
                                <label for="payslip_code" class="control-label">Komponen Gaji</label>
                                <select class="form-control selectto" name="payslip_code" id="payslip_code" required="1" aria-required="true">
                                    <option value="">Silakan Pilih</option>
                                    @foreach ($payslips as $payslip)
                                    <option value="{{ $payslip->payslip_code }}">
                                        {{ $payslip->payslip_desc }}
                                    </option>
                                    @endforeach
                                </select>
                            </div>

                            {{ FormHelper::form('number', 'Persentase', 'percent_fee') }}
                            {{ FormHelper::form('number', 'Jumlah', 'amount_fee') }}
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group" >
                                <label for="type" class="control-label">Tipe</label>
                                <select class="form-control selectto" name="type" id="type" required="1" aria-required="true">
                                    <option value="">Silakan Pilih</option>
                                    <option value="Gaji">Gaji</option>
                                    <option value="Potongan">Potongan</option>
                                    <option value="Dibayar Perusahaan">Dibayar Perusahaan</option>
                                </select>
                            </div>

                            {{ FormHelper::form('textarea', 'Custom Query', 'custom_query') }}

                            <input type="text" name="row" id="row-detail" value="" class="d-none">
                            <input type="text" name="created_date" id="created_date" class="d-none">
                            <input type="text" name="created_by" id="created_by" class="d-none">
                        </div>
                    </div>
                </div>
            </form>

            <div class="modal-footer">
                <button type="button" class="btn btn-success btn-sm" data-dismiss="modal" id="tambah-detail">Tambah</button>
                <button type="button" class="btn btn-success btn-sm" data-dismiss="modal" id="edit-detail" style="display: none;">Edit</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var payroll_header = {!! json_encode($payroll) !!}

    $(document).ready(function(){
        $.each(payroll_header, function(index, value){
            if(index.includes("date")){
                if($('#'+index).data("DateTimePicker") != undefined){
                    $('#'+index).data("DateTimePicker").date(moment(value).format('DD-MMMM-YYYY'));
                }
            }else{
                $('#form-tambah-payroll [name='+index+']').val(value).change();
            }
        });
    });

    $('#btn-tambah-detail').click(function(){
        $('#modal-detail').modal('show');
        $("#form-detail-payroll")[0].reset();
        $('#form-detail-payroll .selectto').val('').change();
        $('#tambah-detail').show();
        $('#edit-detail').hide();
    });

    $('#tambah-detail').click(function(){
        update_detail();
    });

    function update_detail(action = 'insert'){
        var data = $("#form-detail-payroll").serializeArray();
        var payslip_desc = $('#payslip_code').find('option:selected').text();
        var objek = {};
        $.each(data, function(){
            objek[this.name] = this.value
        });

        objek['payslip_desc'] = payslip_desc;

        if(action == 'update'){
            table_payroll.row( $('#payslip_dtl_id').val() ).remove().draw();
        }

        table_payroll.row.add(objek).draw().node();

        $("#form-detail-payroll")[0].reset();
        $('#form-detail-payroll .selectto').val('').change();
    }

    $("form").submit(function(e) {
        e.preventDefault();
        $('input[type=submit]').attr('disabled', 'disabled');
        $.ajax({
            method: "POST",
            url: "{{ route('payroll.store') }}",
            data: new FormData($('#form-tambah-payroll')[0]),
            processData: false,
            contentType: false,
            success: function(response){
                if ($.isNumeric(response)) {
                    alert(response);
                }else{
                    alert(response);
                }
                $('input[type=submit]').attr('disabled', false);
            },
            error: function(response){
                alert(response)
                $('input[type=submit]').attr('disabled', false);
            }
        });

        return true;
    });

    var table_payroll = $('#table-payroll').DataTable({
        paging: false,
        data: {!! json_encode($payroll_detail) !!},
        responsive: true,
        lengthChange: false,
        columns: [
            { data: "payslip_desc" },
            { data: "type" },
            { data: "percent_fee" },
            { data: "amount_fee" },
            { data: "payslip_dtl_id" }
        ],
        columnDefs: [
            // { "visible": false, "targets": 8 },
            {
                render:function(data, type, row){
                    var data_post = {
                        payslip_dtl_id : row['payslip_dtl_id'],
                        payslip_id : row['payslip_id'],
                        payslip_code : row['payslip_code'],
                        type : row['type'],
                        percent_fee : row['percent_fee'],
                        amount_fee : row['amount_fee'],
                        custom_query : row['custom_query'],
                        pay_by : row['pay_by'],
                        created_by : row['created_by'],
                        updated_by : row['updated_by'],
                    }

                    return `
                            <span class="btn btn-danger btn-sm float-right" onclick="remove_line(this)"><i class="fa fa-times"></i></span>
                            <span class="btn btn-warning btn-sm float-right" onclick="edit_line(this)"><i class="fa fa-edit"></i></span>
                            <input type="hidden" name="detail[]" value='`+JSON.stringify(data_post)+`' >
                            `;
                },
                "orderable": false,
                "targets": -1
            },
        ],
    });

    function remove_line(element){
        var data = table_transaksi.rows($(element).parents('tr')).data()[0];
        deleted.push(data['id']);
        $('#deleted').val(JSON.stringify(deleted));
        table_transaksi.row( $(element).parents('tr') ).remove().draw();
        update_header();
    }

    function edit_line(element){
        $('#tambah-detail').hide();
        $('#edit-detail').show();
        var data = table_payroll.rows($(element).parents('tr')).data()[0];
        console.log(data)
        $('#modal-detail').modal('show');

        $("#form-detail-payroll")[0].reset();
        $('#form-detail-payroll .selectto').val('').change();

        insert_form_value('form-detail-payroll', data);
    }
</script>
@stop
