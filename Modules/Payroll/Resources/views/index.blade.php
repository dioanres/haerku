@extends('layouts.app', ['title' => __('Payroll')])

@section('content')
	<div class="py-7">
		<div class="container-fluids">
			<div class="row">
	            <div class="col">
	                <div class="card shadow">
	                    <div class="card-header border-0">
	                        <div class="row align-items-center">
	                            <div class="col-8">
	                                <h3 class="mb-0">Payroll List</h3>
	                            </div>
	                            <div class="col-4 text-right">
	                                <a href="{{route('payroll.create')}}" class="btn btn-sm btn-primary">Add Payroll</a>
	                            </div>
	                        </div>
	                    </div>
	                    <div class="col-sm-12">
	                    	<div class="table-responsive">
		                        <table class="table table-striped table-bordered table-hover dt-responsive nowrap" width="100%" id="table-payroll">
		                            <thead class="primary">
		                                <tr>
		                                    <th>Nama</th>
		                                    <th>Tanggal Mulai</th>
		                                    <th>Tanggal Akhir</th>
		                                    <th>Gaji Bersih</th>
		                                </tr>
		                            </thead>
		                            <tbody>
		                                @foreach ($payrolls as $payroll)
		                                <tr style="cursor: pointer;" onclick="window.location.href = `{{route('payroll.edit', $payroll->payslip_id)}}` ">
		                                	<td>{{$payroll->fullname}}</td>
		                                	<td>{{$payroll->start_date}}</td>
		                                	<td>{{$payroll->end_date}}</td>
		                                	<td>{{$payroll->take_home_pay}}</td>
		                                </tr>
		                                @endforeach
		                            </tbody>
		                        </table>
		                    </div>
	                    </div>
	                    <div class="card-footer py-4">
	                        <nav class="d-flex justify-content-end" aria-label="...">
	                            
	                        </nav>
	                    </div>
	                </div>
	            </div>
	        </div>
		</div>
	</div>

<script type="text/javascript">
	var table_payroll = $('#table-payroll').DataTable({
		responsive: true,
	});
	$(document).ready(function(){
		table_payroll.columns.adjust().responsive.recalc();
	});
</script>
@stop