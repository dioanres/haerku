@extends('layouts.app', ['title' => __('Payroll')])

@section('content')
	<div class="py-7">
		<div class="container-fluids">
			<div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">Payroll Detail</h3>
                            </div>
                            <div class="col-4 text-right">
                                <!-- <a href="http://localhost:88/argon_hr/public/user/create" class="btn btn-sm btn-primary">Add user</a> -->
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-12">
                    
                    </div>

                    
                    <div class="card-footer py-4">
                        <nav class="d-flex justify-content-end" aria-label="...">
                            
                        </nav>
                    </div>
                </div>
            </div>
        </div>
		</div>
	</div>
@stop
