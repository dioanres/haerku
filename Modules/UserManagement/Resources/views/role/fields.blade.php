<fieldset>
    <div class="row">

        <!-- <input type="text" name="employee_info" id="employee_info" class="d-none"> -->
        <div class="col-sm-12">
            {!! Form::label('role_id', 'Role ID:') !!}  
            {!! Form::text('role_id', null, ['class' => 'form-control','readonly' => true]) !!}

            {!! Form::label('role_desc', 'Role Desc:') !!}
            {!! Form::text('role_desc', null, ['class' => 'form-control']) !!} 

        </div>
    </div>
</fieldset>