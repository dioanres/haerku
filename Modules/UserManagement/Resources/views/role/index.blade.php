@extends('layouts.app', ['title' => __('Role')])

@section('content')
	<div class="py-7">
		<div class="container-fluids">
			<div class="row">
	            <div class="col">
	                <div class="card shadow">
	                    <div class="card-header border-0">
	                        <div class="row align-items-center">
	                            <div class="col-8">
	                                <h3 class="mb-0">Role List</h3>
	                            </div>
	                            <div class="col-4 text-right">
	                                <a href="{{route('usermanagement.role.create')}}" class="btn btn-sm btn-primary">Add Role</a>
	                            </div>
	                        </div>
	                    </div>
	                    <div class="col-sm-12">
	                    	<div class="table-responsive">
		                        <table class="table table-striped table-bordered table-hover dt-responsive nowrap" width="100%" id="table-leave">
		                            <thead class="primary">
		                                <tr>
		                                    <th>ID</th>
		                                    <th>Role</th>
		                                    <th>Aksi</th>
		                                </tr>
		                            </thead>
		                            <tbody>
                                    @foreach ($list as $role)
		                                <tr>
                                            <td>{{ $role->role_id }}</td>
                                            <td>{{ $role->role_desc }}</td>
                                            <td>
                                                <a class="btn btn-sm btn-warning" href="{{route('usermanagement.role.edit',['id' => $role->role_id])}}">Edit</a>
                                            </td>
                                        </tr>
                                        @endforeach
		                            </tbody>
		                        </table>
		                    </div>
	                    </div>
	                    <div class="card-footer py-4">
	                        <nav class="d-flex justify-content-end" aria-label="...">
	                            
	                        </nav>
	                    </div>
	                </div>
	            </div>
	        </div>
		</div>
	</div>

<script type="text/javascript">
	var table_leave = $('#table-leave').DataTable({
		responsive: true,
	});
	$(document).ready(function(){
		table_leave.columns.adjust().responsive.recalc();
	});
</script>
@stop