<fieldset>
    <div class="row">

        <!-- <input type="text" name="employee_info" id="employee_info" class="d-none"> -->
        <div class="col-sm-12">
            {!! Form::label('id_module', 'Module ID:') !!}  
            {!! Form::text('id_module', null, ['class' => 'form-control','readonly' => true]) !!}

            {!! Form::label('module_name', 'Module Name:') !!}
            {!! Form::text('module_name', null, ['class' => 'form-control']) !!} 

        </div>
    </div>
</fieldset>