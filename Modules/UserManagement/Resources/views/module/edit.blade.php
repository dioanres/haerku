@extends('layouts.app', ['title' => __('Edit Data Company')])

@section('content')
<div class="py-7">
    <div class="container-fluids">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="col-12 mt-3">
                    
                    {!! Form::model($data, ['route' => ['usermanagement.module.update', collect($data)->first() ], 'method' => 'patch','files' => true]) !!}
                        <p>Module Info :</p>
                        <hr>
                            {{ csrf_field() }}
                            @include('usermanagement::module.fields')
                            <div class="card-footer py-4">
                        <input class="btn btn-success fixed-save" type="submit" id="save-data" value="SIMPAN">
                        <nav class="d-flex justify-content-end" aria-label="...">
                            
                        </nav>
                    </div>
                        {!! Form::close() !!}
                    </div>
 
                    
                </div>
            </div>
        </div>
    </div>
</div>
@stop
