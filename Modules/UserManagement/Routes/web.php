<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::prefix('usermanagement')->middleware('auth')->group(function () {
    Route::get('/', 'UserManagementController@index');
    Route::prefix('role')->group(function () {
        Route::get('/', 'RoleController@index')->name('usermanagement.role');
        Route::get('/create', 'RoleController@create')->name('usermanagement.role.create');
        Route::get('/{id}/edit', 'RoleController@edit')->name('usermanagement.role.edit');
        Route::post('/store', 'RoleController@store')->name('usermanagement.role.store');
    });

    Route::prefix('module')->group(function () {
        Route::get('/', 'ModuleController@index')->name('usermanagement.module');
        Route::get('/create', 'ModuleController@create')->name('usermanagement.module.create');
        Route::get('/{id}/edit', 'ModuleController@edit')->name('usermanagement.module.edit');
        Route::get('/{id}/delete', 'ModuleController@destroy')->name('usermanagement.module.delete');
        Route::post('/store', 'ModuleController@store')->name('usermanagement.module.store');
        Route::patch('/{id}/update', 'ModuleController@update')->name('usermanagement.module.update');
        //Route::put('/{id}/update', 'ModuleController@update')->name('usermanagement.module.update');
    });

});
