<?php

namespace Modules\UserManagement\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ModuleModel extends Model
{
    use SoftDeletes;

    public $timestamps = true;

    protected $table = 'mst_module';

    protected $primaryKey='id_module';

    protected $fillable = [
        'module_name'
    ];


}
