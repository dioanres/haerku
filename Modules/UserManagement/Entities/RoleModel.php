<?php

namespace Modules\UserManagement\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RoleModel extends Model
{
    use SoftDeletes;

    public $timestamps = true;

    protected $table = 'mst_role';

    protected $fillable = [
        'role_desc',
        'created_date'
    ];
}
