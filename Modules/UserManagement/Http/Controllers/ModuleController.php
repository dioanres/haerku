<?php

namespace Modules\UserManagement\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\UserManagement\Entities\ModuleModel;

class ModuleController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {

        $list_data = ModuleModel::get();
        
        return view('usermanagement::module.index')->with(['list_data'=>$list_data]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('usermanagement::module.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        if(ModuleModel::create($input)) {
            return redirect(route('usermanagement.module'));
        } else {
            return redirect(route('usermanagement.module.create'));
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('usermanagement::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $data = ModuleModel::where('id_module',$id)->first();
        return view('usermanagement::module.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        
        $update = ModuleModel::where('id_module',$id)->update([
            'module_name' => $data['module_name'],
        ]);
        
        if($update) {
            return redirect(route('usermanagement.module'));
        } else {
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {   
        ModuleModel::destroy($id);
        return redirect(route('usermanagement.module'));
    }
}
