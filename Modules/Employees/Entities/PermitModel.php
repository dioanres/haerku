<?php

namespace Modules\Employees\Entities;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use Session;
use DB;

class PermitModel extends Model
{
	 public $table = 'permit';

    protected $fillable = [
       'request_no', 
       'permit_date', 
       'permit_type_desc', 
       'approval_desc'];

	public static function list()
    {
        return DB::select("select ph.request_no, ph.permit_date, mpt.permit_type_desc, mas.approval_desc from permit_header ph, permit_approval pa, mst_permit_type mpt, mst_approval_status mas where ph.permit_id = pa.permit_id and ph.permit_type_id = mpt.permit_type_code  and pa.approval_status = mas.approval_status");
    }
}
