<?php

namespace Modules\Employees\Entities;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use Session;
use DB;

class LeaveModel extends Model
{
    public $table = 'leave_header';

    protected $fillable = [
        'request_no',
        'request_date',
        'id_employee',
        'approval_status',
        'created_by',
        'updated_by',
        'leave_code',
        'start_date',
        'end_date',
        'total',
        'reason',
        'file',
    ];

    protected $primaryKey = 'leave_id';
    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'updated_date';

    public static function list()
    {
        return DB::select("select lh.request_no,ml.leave_desc,lh.start_date,lh.end_date,lh.total,mas.approval_desc
            from leave_header lh, mst_leave ml, leave_approval la, mst_approval_status mas
            where lh.leave_code = ml.leave_code
            and lh.leave_id = la.leave_id
            and la.approval_status = mas.approval_status ;");
    }

    public static function insert($params=array(),$userid=0){
        // echo "<pre>";
        // var_dump($params);
        // exit();
        $leave_id = isset($params["leave_id"])?$params["leave_id"]:0;
        $request_no = isset($params["request_no"])?$params["request_no"]:'';
        $id_employee = isset($params["id_employee"])?$params["id_employee"]:'';
        $approval_status = isset($params["approval_status"])?$params["approval_status"]:'';
        $leave_code = isset($params["leave_code"])?$params["leave_code"]:'';
        $start_date = isset($params["start_date"])?$params["start_date"]:date('Y-m-d H:i:s');
        $end_date = isset($params["end_date"])?$params["end_date"]:date('Y-m-d H:i:s');
        $total = isset($params["total"])?$params["total"]:0;
        $reason = isset($params["reason"])?$params["reason"]:'';
        $file = isset($params["file"])?$params["file"]:'';

        $data=array(
            "leave_id"=>$leave_id,
            "request_no"=>$request_no,
            "request_date"=>date('Y-m-d H:i:s'),
            "id_employee"=>$userid,
            "approval_status"=>$approval_status,
            "created_date"=>date('Y-m-d H:i:s'),
            "created_by"=>$userid,
            "leave_code"=>$leave_code,
            "start_date"=>$start_date,
            "end_date"=>$end_date,
            "total"=>$total,
            "reason"=>$reason,
            "file"=>$file
        );
        DB::table('leave_header')->insert($data);
    }
}
