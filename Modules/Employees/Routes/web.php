<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('employees')->middleware('auth')->group(function () {
    // Route::get('/', 'EmployeesController@index')->name('employees');
    //LEAVE
    Route::prefix('leave')->group(function () {
        Route::get('/', 'LeaveController@index')->name('leave');
        Route::get('/create', ['as' => 'leave.create', 'uses' => 'LeaveController@create']);
        Route::post('/insertData', ['as' => 'leave.insertData', 'uses' => 'LeaveController@store']);
        Route::get('/{leave}/edit', ['as' => 'leave.edit', 'uses' => 'LeaveController@edit']);
        Route::post('/store', ['as' => 'leave.store', 'uses' => 'LeaveController@store']);

        Route::get('/personal_info','EmployeesController@personal_info')->name('personal_info');
        Route::get('/employment_info','EmployeesController@employment_info')->name('employment_info');

        Route::get('/list','LeaveController@get_datatable');
    });

    //PERMIT
    Route::prefix('permit')->group(function () {
        Route::get('/', 'PermitController@index')->name('permit');
        Route::get('/create', ['as' => 'permit.create', 'uses' => 'PermitController@create']);
    });

    //PERMIT
    Route::prefix('perdin')->group(function () {
        Route::get('/', 'PerdinController@index')->name('perdin');
        Route::get('/create', ['as' => 'perdin.create', 'uses' => 'PerdinController@create']);
    });

    // Route::prefix('employees_info')->group(function () {
    //     Route::get('/employees_info','EmployeesController@impl_info')->name('employees_info');
    //     Route::get('/create', ['as' => 'employees_info.create', 'uses' => 'EmployeesController@create_impl_info']);
    // });

    Route::prefix('paysplip')->group(function () {
        Route::get('/payslip','EmployeesController@payslip')->name('payslip');
        // Route::get('/create', ['as' => 'payslip.create', 'uses' => 'EmployeesController@create_impl_info']);
    });

});

