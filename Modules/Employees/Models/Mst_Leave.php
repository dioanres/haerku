<?php

namespace Modules\Employees\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use Session;
use DB;


class Mst_Leave extends Model
{
    public $table = 'mst_leave';

    public $fillable = [
        'leave_code',
        'leave_desc',
        'leave_type_code',
        'duration',
        'deleted',
        'created_date',
        'created_by',
        'updated_date',
        'updated_by',
    ];

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'updated_date';
    protected $primaryKey = 'leave_code';

    public static function list(){
        return DB::select("SELECT * from mst_leave a where a.deleted = 0");
    }
}
