<?php

namespace Modules\Employees\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use Session;
use DB;


class Mst_Permit_Type extends Model
{
    public $table = 'mst_permit_type';

    public $fillable = [
        'permit_type_code',
        'permit_type_desc',
        'deleted',
        'created_date',
        'created_by',
        'updated_date',
        'updated_by'
    ];

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'updated_date';
    protected $primaryKey = 'leave_code';

    public static function list(){
        return DB::select("SELECT * from mst_permit_type a where a.deleted = 0");
    }
}
