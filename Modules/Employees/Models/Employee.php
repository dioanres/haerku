<?php

namespace Modules\Employees\Models;

use Illuminate\Database\Eloquent\Model;

use Session;
use DB;


class Employee extends Model
{
    public $table = 'employee';

    public $fillable = [
        'nik',
        'fullname',
        'place_of_birth',
        'date_of_birth',
        'religion_id',
        'marital_status_id',
        'gender',
        'mother_maiden_name',
        'email',
        'hp_no',
        'telp_no',
        'empl_status_id',
        'empl_type_id',
        'joint_date',
        'resign_date',
        'created_date',
        'created_by',
        'updated_date',
        'updated_by',
    ];

    public static function list()
    {
        return DB::select("SELECT * from employee e");
    }
}
