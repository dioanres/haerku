@extends('layouts.app', ['title' => __('Personal Info')])

@section('content')
<div class="py-7">
    <div class="container-fluids">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="col-12 mt-3">
                        <form id="form-tambah-payroll" method="POST" enctype="multipart/form-data" action="#">
                            {{ csrf_field() }}
                            <fieldset>
                                <div class="row">
                                    <input type="text" name="payslip_id" id="payslip_id" class="d-none">
                                    <div class="col-sm-6">
                                    	{{ FormHelper::form('number', 'NIK', 'empl_pi_nik', ['readonly', 'required']) }}
                                        {{ FormHelper::form('text', 'Name ', 'empl_pi_name', ['required']) }}
                                        {{ FormHelper::form('text', 'Place of Birth ', 'empl_place_birth', ['required']) }}
                                        {{ FormHelper::form('datepicker', 'Birth Date', 'empl_birth_date', ['required']) }}
                                        <div class="form-group" >
                                            <label for="nik" class="control-label">Gender</label>
                                            <select class="form-control selectto" name="Jenis Identitas Employee" id="empl_gender" required="1" aria-required="true" readonly>
                                             <!-- List Gender dari DB -->
                                            </select>
                                        </div>
                                        <div class="form-group" >
                                            <label for="nik" class="control-label">Religion</label>
                                            <select class="form-control selectto" name="Jenis Identitas Employee" id="empl_religion" required="1" aria-required="true">
                                             <!-- List JReligion dari DB -->
                                            </select>
                                        </div>
                                        <div class="form-group" >
                                            <label for="nik" class="control-label">Identity Type</label>
                                            <select class="form-control selectto" name="Jenis Identitas Employee" id="empl_id_type" required="1" aria-required="true">
                                             <!-- List Jenis Identitas dari DB -->
                                            </select>
                                        </div>
                                        {{ FormHelper::form('text', 'NO ID', 'empl_id_no', ['required']) }}
                                    </div>
                                    
                                    <div class="col-sm-6">
                                        <div class="form-group" >
                                            <label for="nik" class="control-label">Marital Status</label>
                                            <select class="form-control selectto" name="Jenis Identitas Employee" id="empl_marital_status" required="1" aria-required="true">
                                             <!-- List Marital Status dari DB -->
                                            </select>
                                        </div>
                                        <div class="form-group" >
                                            <label for="nik" class="control-label">Blood Type </label>
                                            <select class="form-control selectto" name="Jenis Identitas Employee" id="empl_blood_type" required="1" aria-required="true">
                                             <!-- List Blood Type dari DB -->
                                            </select>
                                        </div>
                                        {{ FormHelper::form('text', 'Email', 'empl_email', ['required']) }}
                                        {{ FormHelper::form('number', 'Mobile Phone', 'empl_no_phone', ['required']) }}
                                        <div class="form-group">
                                            <label class="control-label" for="custom_query">Citizen ID Address</label>
                                            <div class="">
                                                <textarea class="form-control resize_vertical" id="reason" name="reason" rows="7"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label" for="custom_query">Residential Address</label>
                                            <div class="">
                                                <textarea class="form-control resize_vertical" id="reason" name="reason" rows="7"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <input class="btn btn-success btn-sm fixed-save" type="submit" id="Req_change_data_personal" name="Request Change Data">
                                </div>
                            </fieldset>
                        </form>
                    </div>

                    <div class="col-12 mt-3">
                        <form id="form-tambah-payroll" method="POST" enctype="multipart/form-data" action="#">
                            <p>Emergency Contact :</p>
                            <hr>
                            {{ csrf_field() }}
                            <fieldset>
                                <div class="row">
                                    <input type="text" name="payslip_id" id="payslip_id" class="d-none">
                                    <div class="col-sm-6">
                                        {{ FormHelper::form('number', 'Emergency Contact', 'empl_ec', ['readonly', 'required']) }}
                                        {{ FormHelper::form('text', 'EC Relationship ', 'empl_ec_relationship', ['required']) }}
                                        {{ FormHelper::form('text', 'EC Mobile Phone ', 'empl_ec_phone', ['required']) }}

                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
 
                    <div class="card-footer py-4">
                        <nav class="d-flex justify-content-end" aria-label="...">
                            
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
