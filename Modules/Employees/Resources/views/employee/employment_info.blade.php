@extends('layouts.app', ['title' => __('Employment Info')])

@section('content')
<div class="py-7">
    <div class="container-fluids">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="col-12 mt-3">
                        <form id="form-tambah-payroll" method="POST" enctype="multipart/form-data" action="#">
                            {{ csrf_field() }}
                            <fieldset>
                                <div class="row">
                                    <input type="text" name="payslip_id" id="payslip_id" class="d-none">
                                    <div class="col-sm-6">
                                        {{ FormHelper::form('number', 'NIK', 'empl_ei_nik', ['readonly', 'required']) }}
                                        {{ FormHelper::form('text', 'Name', 'empl_ei_name', ['readonly','required']) }}
                                        {{ FormHelper::form('text', 'Devison', 'empl_devision', ['readonly','required']) }}
                                        {{ FormHelper::form('text', 'Department', 'empl_dep', ['readonly','required']) }}
                                        {{ FormHelper::form('text', 'Department Head ', 'empl_dep_head', ['readonly','required']) }}
                                        {{ FormHelper::form('text', 'Job Position', 'empl_job', ['readonly','required']) }}
                                    </div>
                                    
                                    <div class="col-sm-6">
                                        {{ FormHelper::form('text', 'Job Level', 'empl_level', ['readonly','required']) }}
                                        {{ FormHelper::form('text', 'Employement Status', 'empl_employement_status', ['readonly','required']) }}
                                    
                                        <!-- <div class="form-group" >
                                            <label for="nik" class="control-label">Branch</label>
                                            <select class="form-control selectto" name="Jenis Identitas Employee" id="empl_branch" required="1" aria-required="true">
                                            </select>
                                        </div>
 -->
                                        {{ FormHelper::form('text', 'Join Date', 'empl_ejoin_date', ['readonly','required']) }}
                                        {{ FormHelper::form('text', 'End Probation Date', 'empl_end_Probation', ['readonly','required']) }}
                                        {{ FormHelper::form('text', 'Length Of Service', 'empl_length_service', ['readonly','required']) }}
                                    </div>
                                     <!-- <input class="btn btn-success btn-sm fixed-save" type="submit" id="save-data" name="SIMPAN"> -->
                                </div>
                            </fieldset>
                        </form>
                    </div>
 
                    <div class="card-footer py-4">
                        <nav class="d-flex justify-content-end" aria-label="...">
                            
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
