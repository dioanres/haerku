@extends('layouts.app', ['title' => __('input Data Employee')])

@section('content')
<div class="py-7">
    <div class="container-fluids">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="col-12 mt-3">
                        <form id="form-tambah-payroll" method="POST" enctype="multipart/form-data" action="#">
                        <p>Employee Info :</p>
                        <hr>
                            {{ csrf_field() }}
                            <fieldset>
                                <div class="row">
                                    <!-- <input type="text" name="employee_info" id="employee_info" class="d-none"> -->
                                    <div class="col-sm-6">
                                    	{{ FormHelper::form('number', 'NIK', 'adm_empl_nik', ['readonly', 'required']) }}
                                        {{ FormHelper::form('text', 'Name ', 'adm_empl_name', ['required']) }}
                                        {{ FormHelper::form('text', 'Place of Birth ', 'adm_empl_place_birth', ['required']) }}
                                        {{ FormHelper::form('datepicker', 'Birth Date', 'adm_empl_birth_date', ['required']) }}
                                        <div class="form-group" >
                                            <label for="nik" class="control-label">Gender</label>
                                            <select class="form-control selectto" name="Jenis Identitas Employee" id="adm_empl_gender" required="1" aria-required="true">
                                                <option value="">Pilih Karyawan</option>
                                                <option value="1">Male</option>
                                                <option value="2">Fimale</option>
                                            </select>
                                        </div>
                                        <div class="form-group" >
                                            <label for="nik" class="control-label">Religion</label>
                                            <select class="form-control selectto" name="Jenis Identitas Employee" id="adm_empl_religion" required="1" aria-required="true">
                                             <option value="">Pilih Religion</option>
                                                @foreach ($religion as $religion)
                                                <option value="{{ $religion->religion_id }}">
                                                    {{ $religion->religion_desc }}
                                                </option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group" >
                                            <label for="nik" class="control-label">Identity Type</label>
                                            <select class="form-control selectto" name="Jenis Identitas Employee" id="adm_empl_id_type" required="1" aria-required="true">
                                             <option value="">Pilih Identitas</option>
                                                @foreach ($identity_type as $identity_type)
                                                <option value="{{ $identity_type->identity_type_id }}">
                                                    {{ $identity_type->identity_type }}
                                                </option>
                                                @endforeach
                                            </select>
                                        </div>
                                        {{ FormHelper::form('text', 'NO ID', 'adm_empl_id_no', ['required']) }}
                                    </div>
                                    
                                    <div class="col-sm-6">
                                        <div class="form-group" >
                                            <label for="nik" class="control-label">Marital Status</label>
                                            <select class="form-control selectto" name="Jenis Identitas Employee" id="adm_empl_marital_status" required="1" aria-required="true">
                                             <option value="">Pilih Marital Status</option>
                                                @foreach ($marital_status as $marital_status)
                                                <option value="{{ $marital_status->marital_status_id }}">
                                                    {{ $marital_status->marital_status_desc }}
                                                </option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <!-- <div class="form-group" >
                                            <label for="nik" class="control-label">Blood Type </label>
                                            <select class="form-control selectto" name="Jenis Identitas Employee" id="adm_empl_blood_type" required="1" aria-required="true">
                                            </select>
                                        </div> -->
                                        {{ FormHelper::form('text', 'Blood Type', 'adm_empl_blood_type') }}
                                        {{ FormHelper::form('text', 'Email', 'adm_empl_email', ['required']) }}
                                        {{ FormHelper::form('number', 'Mobile Phone', 'adm_empl_no_phone', ['required']) }}
                                        <div class="form-group">
                                            <label class="control-label" for="custom_query">Citizen ID Address</label>
                                            <div class="">
                                                <textarea class="form-control resize_vertical" id="adm_empl_citizen_addr" name="reason" rows="7"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label" for="custom_query">Residential Address</label>
                                            <div class="">
                                                <textarea class="form-control resize_vertical" id="adm_empl_res_addr" name="reason" rows="7"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            
                        </form>
                    </div>

                    <div class="col-12 mt-3">
                        <form id="form-tambah-payroll" method="POST" enctype="multipart/form-data" action="#">
                            <p>Emergency Contact :</p>
                            <hr>
                            {{ csrf_field() }}
                            <fieldset>
                                <div class="row">
                                    <div class="col-sm-6">
                                    	{{ FormHelper::form('text', 'Emergency Contact', 'adm_empl_ec', ['required']) }}
                                        {{ FormHelper::form('text', 'EC Relationship ', 'adm_empl_ec_relationship', ['required']) }}
                                        {{ FormHelper::form('number', 'EC Mobile Phone ', 'adm_empl_ec_phone', ['required']) }}

                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                    
                    <div class="col-12 mt-3">
                        <form id="form-tambah-payroll" method="POST" enctype="multipart/form-data" action="#">
                            <p>Employement Info :</p>
                            <hr>
                            {{ csrf_field() }}
                            <fieldset>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group" >
                                            <label for="nik" class="control-label">Devison</label>
                                            <select class="form-control selectto" name="Jenis Identitas Employee" id="adm_empl_devision" required="1" aria-required="true">
                                             <option value="">Pilih Devision</option>
                                                @foreach ($division as $division)
                                                <option value="{{ $division->division_id }}">
                                                    {{ $division->division_desc }}
                                                </option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group" >
                                            <label for="nik" class="control-label">Department</label>
                                            <select class="form-control selectto" name="Jenis Identitas Employee" id="adm_empl_dep" required="1" aria-required="true">
                                            <option value="">Pilih Departement</option>
                                                @foreach ($departement as $departement)
                                                <option value="{{ $departement->dept_id }}">
                                                    {{ $departement->dept_desc }}
                                                </option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group" >
                                            <label for="nik" class="control-label">Department Head</label>
                                            <select class="form-control selectto" name="Jenis Identitas Employee" id="adm_empl_dep_head" required="1" aria-required="true">
                                             <!-- List Jenis Identitas dari DB -->
                                            </select>
                                        </div>
                                        <div class="form-group" >
                                            <label for="nik" class="control-label">Job Position</label>
                                            <select class="form-control selectto" name="Jenis Identitas Employee" id="adm_empl_job" required="1" aria-required="true">
                                             <!-- List Jenis Identitas dari DB -->
                                            </select>
                                        </div>
                                         <div class="form-group" >
                                            <label for="nik" class="control-label">Job Level</label>
                                            <select class="form-control selectto" name="Jenis Identitas Employee" id="adm_empl_level" required="1" aria-required="true">
                                            <option value="">Pilih Job Level</option>
                                                @foreach ($job_levels as $job_levels)
                                                <option value="{{ $departement->dept_id }}">
                                                    {{ $job_levels->job_level_desc }}
                                                </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    
                                    <div class="col-sm-6">
                                        <div class="form-group" >
                                            <label for="nik" class="control-label">Employement Status</label>
                                            <select class="form-control selectto" name="Jenis Identitas Employee" id="adm_empl_employement_status" required="1" aria-required="true">
                                             <!-- List Marital Status dari DB -->
                                            </select>
                                        </div>
                                        <div class="form-group" >
                                            <label for="nik" class="control-label">Branch</label>
                                            <select class="form-control selectto" name="Jenis Identitas Employee" id="adm_empl_branch" required="1" aria-required="true">
                                             <!-- List Blood Type dari DB -->
                                            </select>
                                        </div>
                                        {{ FormHelper::form('datepicker', 'Join Date', 'adm_empl_ejoin_date', ['required']) }}
                                        {{ FormHelper::form('datepicker', 'End Probation Date', 'adm_empl_end_Probation', ['required']) }}
                                        {{ FormHelper::form('text', 'Length Of Service', 'adm_empl_length_service', ['readonly','required']) }}
                                       
                                    </div>
                                     <input class="btn btn-success fixed-save" type="submit" id="save-data" value="SIMPAN">
                                </div>
                            </fieldset>
                        </form>
                    </div>
 
                    <div class="card-footer py-4">
                        <nav class="d-flex justify-content-end" aria-label="...">
                            
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
