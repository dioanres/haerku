@extends('layouts.app', ['title' => __('Leave')])

@section('content')
<div class="py-7">
    <div class="container-fluids">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-12">
                                <h3 class="mb-0">Leave Request</h3>
                                <hr>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 mt-3">
                        <form id="form-tambah-leave" method="POST" enctype="multipart/form-data" action="#">
                            {{ csrf_field() }}
                            <fieldset>
                                <div class="row">
                                    <input type="text" name="leave_id" id="leave_id" class="d-none">

                                    <div class="col-sm-6">

                                        {{ FormHelper::form('text', 'Request No', 'request_no', ['readonly', 'required']) }}
                                        {{ FormHelper::form('file', 'Supporting Document', 'file') }}

                                    </div>
                                    <div class="col-sm-6">

                                        {{ FormHelper::form('number', 'Total', 'total', ['readonly']) }}
                                        <div class="form-group">
                                            <label class="control-label" for="custom_query">Reason</label>
                                            <div class="">
                                                <textarea class="form-control resize_vertical" id="reason" name="reason" rows="7"></textarea>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </fieldset>

                            <div class="row">
                                <div class="col-sm-12">
                                    <span class="btn btn-success btn-sm float-right" id="btn-tambah-detail">Detail Cuti</span>
                                </div>
                            </div>

                            <div class="row" style="margin-top: 10px;">
                                <div class="col-sm-12">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered dt-responsive nowrap table-sm" width="100%" id="table-detail">
                                            <thead class="primary">
                                            <tr>
                                                <th>Leave Type</th>
                                                <th>Start Date</th>
                                                <th>End Date</th>
                                                <th></th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <input class="btn btn-success fixed-save" type="submit" id="save-leave" name="SIMPAN">

                        </form>
                    </div>

                    <div class="card-footer py-4">
                        <nav class="d-flex justify-content-end" aria-label="...">

                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-detail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Komponen Gaji</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="form-detail-payroll">
                <input type="text" name="payslip_dtl_id" id="payslip_dtl_id" class="d-none">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group" >
                                <label for="leave_code" class="control-label">Leave Type</label>
                                <select class="form-control selectto" name="leave_code" id="leave_code" required="1" aria-required="true">
                                    <option value="">Choose Leave</option>
                                    @foreach ($mstLeave as $leave)
                                        <option value="{{ $leave->leave_code }}">
                                            {{ $leave->leave_desc }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>

                            {{ FormHelper::form('datepicker', 'Start Date', 'start_date', ['required']) }}
                            {{ FormHelper::form('datepicker', 'End Date', 'end_date', ['required']) }}
                        </div>
                        <div class="col-sm-6">
                            <input type="text" name="row" id="row-detail" value="" class="d-none">
                            <input type="text" name="created_date" id="created_date" class="d-none">
                            <input type="text" name="created_by" id="created_by" class="d-none">
                        </div>
                    </div>
                </div>
            </form>

            <div class="modal-footer">
                <button type="button" class="btn btn-success btn-sm" data-dismiss="modal" id="tambah-detail">Tambah</button>
                <button type="button" class="btn btn-success btn-sm" data-dismiss="modal" id="edit-detail" style="display: none;">Edit</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('#btn-tambah-detail').click(function(){
        $('#modal-detail').modal('show');
        $("#form-detail-payroll")[0].reset();
        $('#form-detail-payroll .selectto').val('').change();
        $('#tambah-detail').show();
        $('#edit-detail').hide();
    });
</script>
@stop
