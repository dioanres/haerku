@extends('layouts.app', ['title' => __('Leave')])

@section('content')
	<div class="py-7">
		<div class="container-fluids">
			<div class="row">
	            <div class="col">
	                <div class="card shadow">
	                    <div class="card-header border-0">
	                        <div class="row align-items-center">
	                            <div class="col-8">
	                                <h3 class="mb-0">Leave List</h3>
	                            </div>
	                            <div class="col-4 text-right">
	                                <a href="{{route('leave.create')}}" class="btn btn-sm btn-primary">Add Leave</a>
	                            </div>
	                        </div>
	                    </div>
	                    <div class="col-sm-12">
	                    	<div class="table-responsive">
		                        <table class="table table-striped table-bordered table-hover dt-responsive nowrap" width="100%" id="table-leave">
		                            <thead class="primary">
		                                <tr>
		                                    <th>Request ID</th>
		                                    <th>Leave Name</th>
		                                    <th>Start Date</th>
		                                    <th>End Date</th>
		                                    <th>Status Request</th>
		                                    <th>Action</th>
		                                </tr>
		                            </thead>
		                            <tbody>
		                                 {{--@foreach ($list as $leave)
		                                <tr>
                                            <td>{{ $leave->request_no }}</td>
                                            <td>{{ $leave->leave_desc }}</td>
                                            <td>{{ $leave->start_date }}</td>
                                            <td>{{ $leave->end_date }}</td>
                                            <td>{{ $leave->total }}</td>
                                            <td>{{ $leave->approval_desc}}</td>
                                            <td>
                                                <a class="btn btn-sm btn-warning" href="#">Edit</a>
                                            </td>
                                        </tr>
                                        @endforeach--}}
		                            </tbody>
		                        </table>
		                    </div>
	                    </div>
	                    <div class="card-footer py-4">
	                        <nav class="d-flex justify-content-end" aria-label="...">

	                        </nav>
	                    </div>
	                </div>
	            </div>
	        </div>
		</div>
	</div>

<script type="text/javascript">
	var table_leave = $('#table-leave').DataTable({
		responsive: true,
        processing: true,
        serverSide: true,
        ajax: {
            url: '{{ url("employees/leave/list") }}'
        },
        columns: [
            {data: 'request_no', name: 'request_no'},
            {data: 'reason', name: 'reason'},
            {data: 'start_date', name: 'start_date'},
            {data: 'end_date', name: 'end_date'},
            {data: 'approval_status', name: 'approval_status'},
            {data: 'leave_id', name: 'leave_id'},
        ],
        scrollX: true,
        scrollY: '450px',
        scrollCollapse: true,
        columnDefs: [
            { orderable: false, targets: [-1] },
            {
                render:function(data, type, row){
                    if(Date.parse(data))
                        return '<span class="d-none">'+data+'</span>' + moment(data).format('DD MMMM YYYY');
                    return '';
                },
                "targets": [2,3]
            },
        ]
	});
	$(document).ready(function(){
		table_leave.columns.adjust().responsive.recalc();
	});
</script>
@stop
