@extends('layouts.app', ['title' => __('Employees')])

@section('content')
	<div class="py-7">
		<div class="container-fluids">
			<div class="row">
	            <div class="col">
	                <div class="card shadow">
	                    <div class="card-header border-0">
	                        <div class="row align-items-center">
	                            <div class="col-8">
	                                <h3 class="mb-0">Leave List</h3>
	                            </div>
	                            <div class="col-4 text-right">
	                                <a href="{{route('leave.create')}}" class="btn btn-sm btn-primary">Add Leave</a>
	                            </div>
	                        </div>
	                    </div>
	                    <div class="col-sm-12">
	                    	<div class="table-responsive">
		                        <table class="table table-striped table-bordered table-hover dt-responsive nowrap" width="100%" id="table-leave">
		                            <thead class="primary">
		                                <tr>
		                                    <th>Request ID</th>
		                                    <th>Leave Name</th>
		                                    <th>Start Date</th>
		                                    <th>End Date</th>
		                                    <th>Total Days</th>
		                                    <th>Status Request</th>
		                                </tr>
		                            </thead>
		                            <tbody>
		                                <!-- @foreach ($payrolls as $payroll)
		                                <tr style="cursor: pointer;" onclick="window.location.href = `{{route('payroll.edit', $payroll->payslip_id)}}` ">
		                                	<td>{{$payroll->fullname}}</td>
		                                	<td>{{$payroll->start_date}}</td>
		                                	<td>{{$payroll->end_date}}</td>
		                                	<td>{{$payroll->take_home_pay}}</td>
		                                </tr>
		                                @endforeach -->
		                            </tbody>
		                        </table>
		                    </div>
	                    </div>
	                    <div class="card-footer py-4">
	                        <nav class="d-flex justify-content-end" aria-label="...">
	                            
	                        </nav>
	                    </div>
	                </div>
	            </div>
	        </div>
		</div>
	</div>

<script type="text/javascript">
	var table_leave = $('#table-leave').DataTable({
		responsive: true,
	});
	$(document).ready(function(){
		table_leave.columns.adjust().responsive.recalc();
	});
</script>
@stop