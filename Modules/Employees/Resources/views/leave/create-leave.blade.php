@extends('layouts.app', ['title' => __('Leave')])

@section('content')
<div class="py-7">
    <div class="container-fluids">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-12">
                                <h3 class="mb-0">Leave Request</h3>
                                <hr>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 mt-3">
                        <form id="form-tambah-leave" method="POST" enctype="multipart/form-data" action="/employees/leave/insertData">
                            {{ csrf_field() }}
                            <fieldset>
                                <div class="row">
                                    <input type="text" name="leave_id" id="leave_id" class="d-none">

                                    <div class="col-sm-6">

                                        {{ FormHelper::form('text', 'Request No', 'request_no', ['readonly', 'required']) }}
                                        {{ FormHelper::form('datepicker', 'Start Date', 'start_date', ['required']) }}
                                        {{ FormHelper::form('datepicker', 'End Date', 'end_date', ['required']) }}


                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group" >
                                            <label for="leave_code" class="control-label">Leave Type</label>
                                            <select class="form-control selectto" name="leave_code" id="leave_code" required="1" aria-required="true">
                                                <option value="">Choose Leave</option>
                                            @foreach ($mstLeave as $leave)
                                                    <option value="{{ $leave->leave_code }}">
                                                        {{ $leave->leave_desc }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>

                                        {{ FormHelper::form('file', 'Supporting Document', 'file') }}

                                        <div class="form-group">
                                            <label class="control-label" for="custom_query">Reason</label>
                                            <div class="">
                                                <textarea class="form-control resize_vertical" id="reason" name="reason" rows="7"></textarea>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </fieldset>

                            <input class="btn btn-success btn-sm fixed-save" type="submit" id="save-leave" name="SIMPAN">

                        </form>
                    </div>

                    <div class="card-footer py-4">
                        <nav class="d-flex justify-content-end" aria-label="...">

                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    /*$('#btn-tambah-detail').click(function(){
        $('#modal-detail').modal('show');
        $("#form-detail-payroll")[0].reset();
        $('#form-detail-payroll .selectto').val('').change();
        $('#tambah-detail').show();
        $('#edit-detail').hide();
    });*/

    var leave = {!! json_encode($data) !!};

    $("#form-tambah-leave").submit(function(e)
    {
        e.preventDefault();
        $('#save-leave').attr('disabled', 'disabled');
        $.ajax({
            method: "POST",
            url: "{{ route('leave.store') }}",
            data: new FormData($('#form-tambah-leave')[0]),
            processData: false,
            contentType: false,
            success: function(response){
                if(response > 0){
                    location.href = "{{url('employees/leave')}}";
                }
            },
            error: function(response){
                // toastr.error(response)
                $('#save-leave').attr('disabled', false);
            }
        });
        return true;
    });

    $(document).ready(function () {
        insert_form_value('form-tambah-leave', leave);
    });
</script>
@stop
