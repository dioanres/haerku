@extends('layouts.app', ['title' => __('Perdin')])

@section('content')
<div class="py-7">
    <div class="container-fluids">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h1 class="mb-0">Perdin Request</h1>
                            </div>
                        </div>
                    </div>
                    <div class="nav-wrapper col-8 mt-3">
                        <ul class="nav nav-pills nav-fill flex-column flex-md-row" id="tabs-icons-text" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link mb-sm-2 mb-md-0 active" id="create-request-tab" data-toggle="tab" href="#create-request" role="tab" aria-controls="create-request" aria-selected="true"><i class="ni ni-cloud-upload-96 mr-2"></i>Create Request</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link mb-sm-2 mb-md-0" id="create-tab" data-toggle="tab" href="#create" role="tab" aria-controls="create-2" aria-selected="false"><i class="ni ni-bell-55 mr-2"></i>Status Request</a>
                            </li>
                        </ul>
                        <hr>
                    </div>

                    <div class="col-12 mt-3 active tab-pane fade show" role="tabpanel" aria-labelledby="create-request-tab" id="create-request">
                        <form id="form-tambah-perdin" method="POST" enctype="multipart/form-data" action="#">
                            {{ csrf_field() }}
                            <fieldset>
                                <div class="row">
                                    <input type="text" name="leave_id" id="leave_id" class="d-none">

                                    <div class="col-sm-7">

                                        {{ FormHelper::form('text', 'Request No', 'request_no', ['readonly', 'required']) }}
                                        {{ FormHelper::form('datepicker', 'Request Date', 'request_date', ['readonly']) }}
                                        {{ FormHelper::form('text', 'NIK', 'nik', ['readonly', 'required']) }}
                                        {{ FormHelper::form('text', 'Name', 'name', ['readonly']) }}
                                        {{ FormHelper::form('text', 'Division', 'division', ['readonly']) }}
                                        {{ FormHelper::form('text', 'Departement', 'dept', ['readonly']) }}
                                        {{ FormHelper::form('text', 'Project', 'project_name', ['required']) }}

                                        <div class="form-group">
                                            <label class="control-label" for="custom_query">Trip Type</label>
                                            <div>
                                                <div class="custom-control custom-radio custom-control-inline">
                                                  <input type="radio" id="trip-type1" name="trip_type" class="custom-control-input" value="0">
                                                  <label class="custom-control-label" for="customRadioInline1">Domestic</label>
                                                </div>
                                                <div class="custom-control custom-radio custom-control-inline">
                                                  <input type="radio" id="trip-type2" name="trip_type" class="custom-control-input" value="1">
                                                  <label class="custom-control-label" for="customRadioInline2">International</label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group" >
                                            <label for="custom_query" class="control-label">Destination</label>
                                            <select class="form-control selectto" name="country_id" id="country-id" required="1" aria-required="true">
                                                <option value="">Choose Leave</option>
                                               
                                            </select>
                                        </div>

                                        {{ FormHelper::form('datepicker', 'Departure', 'depart_date', ['required']) }}
                                        {{ FormHelper::form('datepicker', 'Arrival', 'arrival_date', ['required']) }}
                                        
                                        <div class="form-group">
                                            <label class="control-label" for="custom_query">Purpose</label>
                                            <div class="">
                                                <textarea class="form-control resize_vertical" id="purpose" name="perdin_purpose" rows="7"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    
                                    <!-- <div class="table-responsive col-sm-7">
                                        <div class="card-header border-0">
                                            <div class="row">
                                                <div class="col-8">
                                                    <h2 class="mb-0">Estimation Cost</h2>
                                                </div>
                                                <div class="col-4 text-right">
                                                    <a href="{{route('perdin.create')}}" class="btn btn-sm btn-primary">Add Cost</a>
                                                </div>
                                            </div>
                                        </div>
                                        <table class="table table-striped table-bordered table-hover dt-responsive nowrap" width="100%" id="table-perdin">
                                            <thead class="primary">
                                                <tr>
                                                    <th>Description</th>
                                                    <th>Qty</th>
                                                    <th>Amount</th>
                                                    <th>Sub Total</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                
                                            </tbody>
                                        </table>
                                    </div>
 -->
                                </div>

                            </fieldset>

                            <div class="row">
                                <div class="col-sm-8">
                                    <h3>Estimation Cost</h3>
                                    <span class="btn btn-success btn-sm float-right" id="btn-tambah-detail">Add Cost</span>
                                </div>
                            </div>
                            <div class="row" style="margin-top: 10px;">
                                <div class="col-sm-8">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered dt-responsive nowrap table-sm" width="100%" id="table-payroll">
                                            <thead class="primary">
                                                <tr>
                                                    <th>Description</th>
                                                    <th>Qty</th>
                                                    <th>Amount</th>
                                                    <th>Sub Total</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            
                            <input class="btn btn-success btn-sm fixed-save" type="submit" id="save-perdin" name="SIMPAN">
                            
                        </form>
                    </div>
                    <div class="col-12 mt-3 tab-pane fade" role="tabpanel" aria-labelledby="create-tab" id="create">
                        asdasfasfasf 
                    </div>
                    
                    <div class="card-footer py-4">
                        <nav class="d-flex justify-content-end" aria-label="...">
                            
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    
</script>
@stop
