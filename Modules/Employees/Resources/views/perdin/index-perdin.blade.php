@extends('layouts.app', ['title' => __('Perdin')])

@section('content')
	<div class="py-7">
		<div class="container-fluids">
			<div class="row">
	            <div class="col">
	                <div class="card shadow">
	                    <div class="card-header border-0">
	                        <div class="row align-items-center">
	                            <div class="col-8">
	                                <h3 class="mb-0">Perdin List</h3>
	                            </div>
	                            <div class="col-4 text-right">
	                                <a href="{{route('perdin.create')}}" class="btn btn-sm btn-primary">Add Perdin</a>
	                            </div>
	                        </div>
	                    </div>
	                    <div class="col-sm-12">
	                    	<div class="table-responsive">
		                        <table class="table table-striped table-bordered table-hover dt-responsive nowrap" width="100%" id="table-perdin">
		                            <thead class="primary">
		                                <tr>
		                                    <th>Request ID</th>
		                                    <th>Perdin Date</th>
		                                    <th>Perdin Type</th>
		                                    <th>Status Request</th>
		                                </tr>
		                            </thead>
		                            <tbody>
		                                
		                            </tbody>
		                        </table>
		                    </div>
	                    </div>
	                    <div class="card-footer py-4">
	                        <nav class="d-flex justify-content-end" aria-label="...">
	                            
	                        </nav>
	                    </div>
	                </div>
	            </div>
	        </div>
		</div>
	</div>

<script type="text/javascript">
	var table_perdin = $('#table-perdin').DataTable({
		responsive: true,
	});
	$(document).ready(function(){
		table_perdin.columns.adjust().responsive.recalc();
	});
</script>
@stop