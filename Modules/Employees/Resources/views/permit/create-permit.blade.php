@extends('layouts.app', ['title' => __('Permit')])

@section('content')
<div class="py-7">
    <div class="container-fluids">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">Permit Request</h3>
                                <hr>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 mt-3">
                        <form id="form-tambah-permit" method="POST" enctype="multipart/form-data" action="#">
                            {{ csrf_field() }}
                            <fieldset>
                                <div class="row">
                                    <input type="text" name="permit_id" id="permit_id" class="d-none">

                                    <div class="col-sm-6">

                                        {{ FormHelper::form('text', 'Request No', 'request_no', ['readonly', 'required']) }}
                                        {{ FormHelper::form('text', 'NIK', 'nik', ['readonly', 'required']) }}
                                        {{ FormHelper::form('text', 'Name', 'name', ['readonly']) }}
                                        {{ FormHelper::form('datepicker', 'Permit Date', 'permit_date', ['required']) }}

                                        <div class="form-group" >
                                            <label for="permit_type_code" class="control-label">Permit Type</label>
                                            <select class="form-control selectto" name="permit_type_code" id="permit_type_code" required="1" aria-required="true">
                                                <option value="">Choose Permit</option>
                                                @foreach ($mstPermitType as $permit)
                                                <option value="{{ $permit->permit_type_code }}">
                                                    {{ $permit->permit_type_desc }}
                                                </option>
                                                @endforeach
                                            </select>
                                        </div>
                                        {{ FormHelper::form('text', 'Hours In', 'hours_in', ['required']) }}
                                        {{ FormHelper::form('text', 'Hours Out', 'hours_out', ['required']) }}
                                        {{ FormHelper::form('number', 'Duration', 'duration', ['readonly']) }}
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group" >
                                            <label for="purpose_code" class="control-label">Purpose</label>
                                            <select class="form-control selectto" name="purpose_code" id="purpose_code" required="1" aria-required="true">
                                                <option value="">Choose Purpose</option>
                                                <!-- @foreach ($mstPermitType as $permit) -->
                                                <!-- <option value="{{ $permit->permit_type_code }}"> -->
                                                    <!-- {{ $permit->permit_type_desc }} -->
                                                <!-- </option> -->
                                                <!-- @endforeach -->
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label" for="custom_query">Reason</label>
                                            <div class="">
                                                <textarea class="form-control resize_vertical" id="reason" name="reason" rows="7"></textarea>
                                            </div>
                                        </div>

                                        {{ FormHelper::form('file', 'Supporting Document', 'file') }}

                                    </div>
                                            
                                </div>
                            </fieldset>
                            
                            <input class="btn btn-success fixed-save" type="submit" id="save-permit" name="SIMPAN">
                            
                        </form>
                    </div>
                    
                    <div class="card-footer py-4">
                        <nav class="d-flex justify-content-end" aria-label="...">
                            
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    
</script>
@stop
