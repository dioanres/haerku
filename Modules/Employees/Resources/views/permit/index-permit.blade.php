@extends('layouts.app', ['title' => __('Permit')])

@section('content')
	<div class="py-7">
		<div class="container-fluids">
			<div class="row">
	            <div class="col">
	                <div class="card shadow">
	                    <div class="card-header border-0">
	                        <div class="row align-items-center">
	                            <div class="col-8">
	                                <h3 class="mb-0">Permit List</h3>
	                            </div>
	                            <div class="col-4 text-right">
	                                <a href="{{route('permit.create')}}" class="btn btn-sm btn-primary">Add Permit</a>
	                            </div>
	                        </div>
	                    </div>
	                    <div class="col-sm-12">
	                    	<div class="table-responsive">
		                        <table class="table table-striped table-bordered table-hover dt-responsive nowrap" width="100%" id="table-permit">
		                            <thead class="primary">
		                                <tr>
		                                    <th>Request ID</th>
		                                    <th>Permit Date</th>
		                                    <th>Permit Type</th>
		                                    <th>Status Request</th>
		                                    <th>Action</th>
		                                </tr>
		                            </thead>
		                            <tbody>
		                                 @foreach ($list as $permit)
		                                <tr>
                                            <td>{{ $permit->request_no }}</td>
                                            <td>{{ $permit->permit_date }}</td>
                                            <td>{{ $permit->permit_type_desc }}</td>
                                            <td>{{ $permit->approval_desc }}</td>
                                            <td>
                                                <a class="btn btn-sm btn-warning" href="#">Edit</a>
                                            </td>
                                        </tr>
                                        @endforeach
		                            </tbody>
		                        </table>
		                    </div>
	                    </div>
	                    <div class="card-footer py-4">
	                        <nav class="d-flex justify-content-end" aria-label="...">
	                            
	                        </nav>
	                    </div>
	                </div>
	            </div>
	        </div>
		</div>
	</div>

<script type="text/javascript">
	var table_permit = $('#table-permit').DataTable({
		responsive: true,
	});
	$(document).ready(function(){
		table_permit.columns.adjust().responsive.recalc();
	});
</script>
@stop