<?php

namespace Modules\Employees\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
// use Modules\Employees\Models\Mst_Permit_Type;
use DB;
use Flash;

class PerdinController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('employees::perdin.index-perdin');
    }

    public function create()
    {
    	// $data['mstPermitType'] = Mst_Permit_Type::list();
        return view('employees::perdin.create-perdin');
    }
}
