<?php

namespace Modules\Employees\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Employees\Models\Mst_Permit_Type;
use Modules\Employees\Entities\PermitModel;
use DB;
use Flash;

class PermitController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $data['list'] = PermitModel::list();
        return view('employees::permit.index-permit',$data);
    }

    public function create()
    {
    	$data['mstPermitType'] = Mst_Permit_Type::list();
        return view('employees::permit.create-permit', $data);
    }
}
