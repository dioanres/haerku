<?php

namespace Modules\Employees\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response; 
use Illuminate\Routing\Controller;
use App\Models\{Mst_Religion,Mst_Identity_Type,Mst_Marital_Status,Mst_Division,Mst_Departement,JobLevelsModel};


class EmployeesController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        dd(25);
        return view('employees::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('employees::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('employees::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('employees::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    // public function impl_info()
    // {
    //     return view('employees::employee.index_employees_info');
    // }

    // public function create_impl_info()
    // {
    //     $data['religion'] = Mst_Religion::all();
    //     $data['identity_type'] = Mst_Identity_Type::all();
    //     $data['marital_status'] = Mst_Marital_Status::all();
    //     $data['division'] = Mst_Division::all();
    //     $data['departement'] = Mst_Departement::all();
    //     $data['job_levels'] = JobLevelsModel::all();
    //     return view('employees::employee.create_employees_info',$data);
    //     // return view('religion::create', $data);
    // }

    public function personal_info()
    {
        return view('employees::employee.personal_info');
    }

    public function employment_info()
    {
        return view('employees::employee.employment_info');
    }

    public function payslip()
    {
        return view('employees::employee.payslip');
    }
}
