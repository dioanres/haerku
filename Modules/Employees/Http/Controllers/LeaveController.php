<?php

namespace Modules\Employees\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Employees\Models\Mst_Leave;
use Modules\Employees\Entities\LeaveModel;
use DB;
use Flash;
use Auth;

class LeaveController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $data['list'] = LeaveModel::list();
        return view('employees::leave.index-leave');
    }

    public function create()
    {
        $data['mstLeave'] = Mst_Leave::list();
        return view('employees::leave.create-leave', $data);
    }

    public function edit($id)
    {
        $data['mstLeave'] = Mst_Leave::list();
        $data['data'] = LeaveModel::find($id);
        return view('employees::leave.create-leave', $data);
    }

    public function store(Request $request)
    {
        $request->merge(['approval_status' => 'Draft']);
        $request->merge(['created_by' => Auth::user()->name]);
        $request->merge(['request_date' => date('Y-m-d')]);
        $request->merge(['request_no' => date('Y-m-d')]);
        $request->merge(['start_date' => date("Y-m-d", strtotime($request->input('start_date')))]);
        $request->merge(['end_date' => date("Y-m-d", strtotime($request->input('end_date')))]);
        $request->merge(['id_employee' => Auth::user()->id]);

        $insert = LeaveModel::updateOrCreate(['leave_id' => $request->input('leave_id')], $request->only([
                'request_no',
                'request_date',
                'id_employee',
                'approval_status',
                'created_by',
                'updated_by',
                'leave_code',
                'start_date',
                'end_date',
                'total',
                'reason',
                'file',
            ]
        ));

        return $insert->leave_id;
    }

    public function insertData(Request $request) {
        $insertDB =  LeaveModel::insert($request->input(),$request->user()->id);
        return view('employees::leave.index-leave');
    }

    public function tes(Request $request){
        // return $request->session()->all();
        return $request->user();
    }

    function get_datatable()
    {
//        $data = Datatables::of(  )->make(true);
        return datatables(LeaveModel::all())->toJson();

    }
}
