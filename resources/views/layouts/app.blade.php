<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Argon Dashboard') }}</title>
        <!-- Favicon -->
        <link href="{{ asset('argon') }}/img/brand/favicon.png" rel="icon" type="image/png">
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
        <!-- Icons -->
        <link href="{{ asset('argon') }}/vendor/nucleo/css/nucleo.css" rel="stylesheet">
        <link href="{{ asset('argon') }}/vendor/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">
        <!-- Argon CSS -->
        <link type="text/css" href="{{ asset('argon') }}/css/argon.css?v=1.0.0" rel="stylesheet">

        <script src="{{ asset('argon') }}/vendor/jquery/dist/jquery.min.js"></script>
        <script src="{{ asset('argon') }}/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment-with-locales.min.js"></script>

        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>

        <!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"> -->
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap4.min.css">
        <!-- <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/responsive/2.2.2/css/responsive.dataTables.min.css"> -->

        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js"></script>
        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap4.min.js"></script>

        <link type="text/css" href="{{ asset('css') }}/datetimepicker.css" rel="stylesheet">
        <script type="text/javascript" src="{{ asset('js') }}/datetimepicker.js"></script>

        <style>
            @media (min-width: 1200px){
                .modal-xl {
                    max-width: 1140px;
                }
            }
        </style>

        <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/eonasdan-bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css" /> -->
        <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/eonasdan-bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" /> -->
        <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/eonasdan-bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script> -->
    </head>
    <body class="{{ $class ?? '' }}">
        @auth()
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
            @include('layouts.navbars.sidebar')
        @endauth

        <div class="main-content">
            @include('layouts.navbars.navbar')
            @yield('content')
        </div>

	<link type="text/css" href="{{ asset('css') }}/custom.css" rel="stylesheet">
        @guest()
            @include('layouts.footers.guest')
        @endguest

        @stack('js')

        <!-- Argon JS -->
        <script src="{{ asset('argon') }}/js/argon.js?v=1.0.0"></script>

        <script type="text/javascript">
            function insert_form_value(form_id, values){
                $.each(values, function(index, value){
                    console.log(value)
                    if($('#'+form_id+' [name='+index+']').attr('type') == 'checkbox')
                    {
                        if(value==1)
                        {
                            $('#'+form_id+' [name='+index+']').prop('checked',true);
                        }
                        else
                        {
                            $('#'+form_id+' [name='+index+']').prop('checked',false);
                        }
                        $('.form-check-input-switch').bootstrapSwitch();
                    }else if($('#'+form_id+' [name='+index+']').hasClass('datetimepicker')){
                        if($('#'+form_id+' [name='+index+']').data("DateTimePicker") != undefined){
                            $('#'+form_id+' [name='+index+']').data("DateTimePicker").date(moment(value).format('DD-MMMM-YYYY'));
                        }
                    }else{
                        $('#'+form_id+' [name='+index+']').val(value).change();
                    }
                });
            }

            $('.datetimepicker').datetimepicker({
                format: 'DD-MMMM-YYYY',
                allowInputToggle: true,
                showTodayButton: true,
                showClear: true,
                ignoreReadonly: true
            });

            $('.datetimepicker-month').datetimepicker({
                format: 'MMMM-YYYY',
                allowInputToggle: true,
                defaultDate: new Date(),
                ignoreReadonly: true
            });

            $('.selectto').select2({
                placeholder: "Silakan Pilih",
                theme: "bootstrap",
                allowClear: true
            });
            $(document).ready(function(){
                $('.modal').on('shown.bs.modal', function () {
                    $(this).find('.modal-body').focus();
                });
            });
        </script>
    </body>
</html>
